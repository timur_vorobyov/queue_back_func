<?php


namespace App\Modules\VisitPurposeType;


use App\Interfaces\IJsonSerializable;

class EVisitPurposeType implements IJsonSerializable
{
    private int $id;
    private string $name;
    private ?object $visitPurposes;

    public function __construct(
        int $id,
        string $name,
        ?object $visitPurposes
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->visitPurposes = $visitPurposes;
    }

    public function superAdminJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "visitPurposes" => $this->visitPurposes ? $this->visitPurposes->map(function ($visitPurpose) {
                return $visitPurpose->toDomainEntity()->superAdminJsonSerialize();
            }) : null
        ];
    }

    public function adminJsonSerialize(): array
    {
        // TODO: Implement adminJsonSerialize() method.
    }

    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "visitPurposes" => $this->visitPurposes ? $this->visitPurposes->map(function ($visitPurpose) {
                return $visitPurpose->toDomainEntity()->jsonSerialize();
            }) : null
        ];
    }
}
