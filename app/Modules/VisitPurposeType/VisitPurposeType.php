<?php


namespace App\Modules\VisitPurposeType;


use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Model;

class VisitPurposeType extends Model
{
    protected $guarded = [];

    public function visitPurposes()
    {
        return $this->hasMany(VisitPurpose::class);
    }

    public function toDomainEntity(): EVisitPurposeType
    {
        $relations = $this->getRelations();

        return new EVisitPurposeType(
            $this->id,
            $this->name,
            key_exists('visitPurposes', $relations) && !is_null($this->visitPurposes) ? $this->visitPurposes : null,
        );
    }
}
