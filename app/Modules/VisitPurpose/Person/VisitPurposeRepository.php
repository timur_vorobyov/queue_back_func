<?php

namespace App\Modules\VisitPurpose\Person;

use App\Modules\VisitPurpose\Person\Interfaces\IVisitPurposeRepository;
use App\Modules\VisitPurpose\VisitPurpose;

class VisitPurposeRepository implements IVisitPurposeRepository
{

    public function getVisitPurposesList(int $branchOfficeId): object
    {
        $visitPurposes = VisitPurpose::with("subVisitPurposes")->whereHas("branchOffices", function ($q) use ($branchOfficeId) {
                $q->where('branch_office_id', $branchOfficeId);

            })->get();
        return $visitPurposes->map(function ($visitPurpose) {
            return $visitPurpose->toDomainEntity()->jsonSerialize();
        });
    }

}
