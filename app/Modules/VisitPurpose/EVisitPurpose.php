<?php

namespace App\Modules\VisitPurpose;

use App\Interfaces\IJsonSerializable;
use App\Modules\VisitPurposeType\EVisitPurposeType;

class EVisitPurpose implements IJsonSerializable
{
    private int $id;
    private string $name;
    private string $series;
    private ?EVisitPurposeType $visitPurposeType;
    private ?object $services;
    private ?object $subVisitPurposes;
    private string $tjMessage;
    private string $ruMessage;
    private bool $isPrioratised;
    private bool $isCashRegister;

    public function __construct(
        int $id,
        string $name,
        string $series,
        ?EVisitPurposeType $visitPurposeType,
        ?object $services,
        ?object $subVisitPurposes,
        string $tjMessage,
        string $ruMessage,
        bool $isPrioratised,
        bool $isCashRegister
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->visitPurposeType = $visitPurposeType;
        $this->series = $series;
        $this->services = $services;
        $this->subVisitPurposes = $subVisitPurposes;
        $this->tjMessage = $tjMessage;
        $this->ruMessage = $ruMessage;
        $this->isPrioratised = $isPrioratised;
        $this->isCashRegister = $isCashRegister;
    }

    public function getSubVisitPurposes()
    {
        return $this->subVisitPurposes;
    }

    public function  getIsPrioratised():bool
    {
        return $this->isPrioratised;
    }

    public function  getIsCashRegister():bool
    {
        return $this->isCashRegister;
    }

    public function getTjMessage(): string
    {
        return  $this->tjMessage;
    }

    public function getRuMessage(): string
    {
        return $this->ruMessage;
    }

    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "series" => $this->series,
            "subVisitPurposes" => $this->subVisitPurposes,
        ];
    }

    public function superAdminJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "series" => $this->series,
            "visitPurposeType" => $this->visitPurposeType ? $this->visitPurposeType->superAdminJsonSerialize() : null,
            "services" => $this->services ? $this->services->map(function ($service) {
                return $service->toDomainEntity()->superAdminJsonSerialize();
            }) : null,
            "tjMessage" => $this->tjMessage,
            "ruMessage" => $this->ruMessage,
            "isPrioratised" => $this->isPrioratised,
            "isCashRegister" => $this->isCashRegister,
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSeries(): string
    {
        return $this->series;
    }

    public function adminJsonSerialize(): array
    {
        // TODO: Implement adminJsonSerialize() method.
    }
}
