<?php

namespace App\Modules\BranchOffice;

use App\Interfaces\IJsonSerializable;
use App\Modules\Town\ETown;

class EBranchOffice implements IJsonSerializable
{
    private int $id;
    private int $townId;
    private string $name;
    private string $initial;
    private ?ETown $town;
    private int $timer;
    private ?object $visitPurposes;

    public function __construct(
        int $id,
        int $townId,
        string $name,
        string $initial,
        int $timer,
        ?ETown $town,
        ?object $visitPurposes
    ) {
        $this->id = $id;
        $this->townId = $townId;
        $this->name = $name;
        $this->initial = $initial;
        $this->town = $town;
        $this->timer = $timer;
        $this->visitPurposes = $visitPurposes;
    }

    public function getVisitPurposesIds() {
        return $this->visitPurposes->get()->pluck('id')->all();
    }

    public function getInitial(): string
    {
        return $this->initial;
    }

    public function getTown(): ?Etown
    {
        return $this->town;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTimer(): int
    {
        return $this->timer;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "townId" => $this->townId,
            "initial" => $this->initial,
            "name" => $this->name,
            "townName" => $this->getTown()->getName(),
        ];
    }

    public function superAdminJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "town" => [
                "id" => $this->town->getId(),
                "name" => $this->town->getName()
                ],
            "initial" => $this->initial,
            "name" => $this->name,
            "timer" => $this->timer,
            "townName" => $this->town->getName(),
            "visitPurposes" => $this->visitPurposes ? $this->visitPurposes->map(function ($visitPurpose) {
                return $visitPurpose->toDomainEntity()->superAdminJsonSerialize();
            }) : null,
        ];
    }

    public function adminJsonSerialize(): array
    {
        // TODO: Implement adminJsonSerialize() method.
    }
}
