<?php

namespace App\Modules\BranchOffice;

use App\Modules\Town\Town;
use App\Modules\VisitPurpose\VisitPurpose;
use Database\Factories\BranchOfficeFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchOffice extends Model
{
    use HasFactory;

    protected $table = "branch_offices";
    public $incrementing = true;
    protected $guarded = [];

    protected static function newFactory()
    {
        return BranchOfficeFactory::new();
    }

    public function town()
    {
        return $this->belongsTo(Town::class);
    }

    public function visitPurposes()
    {
        return $this->belongsToMany(VisitPurpose::class);
    }

    public function toDomainEntity(): EBranchOffice
    {
        return new EBranchOffice(
            $this->id,
            $this->town_id,
            $this->name,
            $this->initial,
            $this->timer,
            $this->town->toDomainEntity(),
            $this->visitPurposes,
        );
    }
}
