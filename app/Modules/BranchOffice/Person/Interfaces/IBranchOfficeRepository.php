<?php

namespace App\Modules\BranchOffice\Person\Interfaces;

interface IBranchOfficeRepository
{
    public function getBranchOfficesFilteredByTownId(int $townId): object;
}
