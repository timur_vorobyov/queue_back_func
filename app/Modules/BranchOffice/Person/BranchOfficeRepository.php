<?php

namespace App\Modules\BranchOffice\Person;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\BranchOffice\Person\Interfaces\IBranchOfficeRepository;


class BranchOfficeRepository implements IBranchOfficeRepository
{

    public function getBranchOfficesFilteredByTownId($townId): object
    {
        $branchOffices = BranchOffice::with('visitPurposes', 'town')->where("town_id", $townId)->get();
        return $branchOffices->map(function ($branchOffice) {
            return $branchOffice->toDomainEntity()->jsonSerialize();
        });
    }
}
