<?php


namespace App\Modules\Reports\ReportUserActionTime;


use App\Modules\User\EUser;

class EReportUserActionTime
{
    private int $id;
    private int $userId;
    private string $breakTime;
    private string $workTime;
    private string $offlineTime;
    private string $lunchTime;
    private string $createdAt;
    private EUser $user;


    public function __construct(
        int $id,
        int $userId,
        string $lunchTime,
        string $breakTime,
        string $workTime,
        string $offlineTime,
        string $createdAt,
        EUser $user
    ) {
        $this->id = $id;
        $this->userId = $userId;
        $this->breakTime = $breakTime;
        $this->workTime = $workTime;
        $this->offlineTime = $offlineTime;
        $this->lunchTime = $lunchTime;
        $this->createdAt = $createdAt;
        $this->user = $user;
    }

    public function reportJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "userFullName" => $this->user->getFullName(),
            "userId"=> $this->userId,
            "breakTime" => $this->breakTime,
            "workTime" => $this->workTime,
            "offlineTime" => $this->offlineTime,
            "lunchTime" => $this->lunchTime,
        ];
    }

    public function exportJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "userFullName" => $this->user->getFullName(),
            "breakTime" => $this->breakTime,
            "workTime" => $this->workTime,
            "offlineTime" => $this->offlineTime,
            "lunchTime" => $this->lunchTime,
            "createdAt" => $this->createdAt
        ];
    }
}
