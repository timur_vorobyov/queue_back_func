<?php

namespace App\Modules\Status;

use Database\Factories\StatusFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public static string $newStatus = "new";
    public static string $invitedStatus = "invited";
    public static string $servingStatus = "serving";
    public static string $completedStatus = "completed";
    public static string $declinedStatus = "declined";

    use HasFactory;

    protected $guarded = [];
    public $timestamps = false;

    protected static function newFactory()
    {
        return StatusFactory::new();
    }

}
