<?php

namespace App\Modules\User\Person\Requests;

class UpdateUserSettingsRequest extends AUserRequest
{
    public function __construct(
        int $userId,
        int $branchOfficeId,
        array $visitPurposesIds,
        string $deskNumber,
        ?int $userIdWithSameDeskNumber
    ) {
        $this->userId = $userId;
        $this->branchOfficeId = $branchOfficeId;
        $this->visitPurposesIds = $visitPurposesIds;
        $this->deskNumber = $deskNumber;
        $this->userIdWithSameDeskNumber = $userIdWithSameDeskNumber;
    }

    public function getUserIdWithSameDeskNumber() {
        return $this->userIdWithSameDeskNumber;
    }

    public function toArray(): array
    {
        return [
            "userId" => $this->userId,
            "branchOfficeId" => $this->branchOfficeId,
            "visitPurposesIds" => $this->visitPurposesIds,
            "deskNumber" => $this->deskNumber,
            "userIdWithSameDeskNumber" => $this->userIdWithSameDeskNumber,
        ];
    }

    public function validationRules(): array
    {
        return [
            "userId" => "required",
            "branchOfficeId" => "required",
            "visitPurposesIds" => "required",
            "deskNumber" => "required",
        ];
    }
}
