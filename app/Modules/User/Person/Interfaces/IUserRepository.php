<?php


namespace App\Modules\User\Person\Interfaces;


use App\Modules\User\EUser;
use App\Modules\User\User;

interface IUserRepository
{
    public function login(string $email, string $password): bool;
    public function getUserByEmail(string $email): EUser;
    public function updateSettingsData(array $newWorkData): EUser;
    public function getAllTicketsFilteredByBranchOfficeId(int $branchOfficeId): object;
    public function getAllTicketsFilteredByUserIdVisitPurposesIds(int $userId, array $visitPurposesIds): object;
    public function getCustomersAmountUserServed(int $userId):string;
    public function getUsersMaxServiceTime(int $branchOfficeId):string|null;
    public function getUserFilteredByDeskNumber(string $deskNumber): EUser;
    public function updateStatus(int $userId, int $statusId): bool;
    public function updateUserActionTimeReport(EUser $userDataBeforeBeingUpdated): bool;
    public function getUserById(int $userId): EUser;
    public function getUserDataFilteredById (int $userId):array;
}
