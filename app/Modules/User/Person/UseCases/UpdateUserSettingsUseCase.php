<?php


namespace App\Modules\User\Person\UseCases;


use App\Modules\User\Person\Events\UserDeskNumberEvent;
use App\Requests\Person\ARequest;

class UpdateUserSettingsUseCase extends AUserUseCase
{

    public function perform(ARequest $request)
    {
        $request->validate();
        $userData = $this->userRepository->updateSettingsData($request->getUserSettingsData());
        if($request->getUserIdWithSameDeskNumber()) {
            $userWithTheSameDeskNumber = $this->userRepository->getUserById($request->getUserIdWithSameDeskNumber());
            event(new UserDeskNumberEvent(
                $userWithTheSameDeskNumber->jsonSerialize()
            ));
        }
        event(new UserDeskNumberEvent($userData->jsonSerialize()));
    }

}
