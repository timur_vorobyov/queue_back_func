<?php

namespace App\Modules\User\Person\UseCases;

use App\Interfaces\IUseCase;
use App\Modules\User\Person\Interfaces\IUserRepository;
use App\Requests\Person\ARequest;

abstract class AUserUseCase implements IUseCase
{
    protected IUserRepository $userRepository;

    public function __construct(IUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
}
