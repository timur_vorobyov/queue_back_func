<?php


namespace App\Modules\User\Person;


use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Reports\ReportUserActionTime\ReportUserActionTime;
use App\Modules\Ticket\Ticket;
use App\Modules\User\EUser;
use App\Modules\User\Person\Interfaces\IUserRepository;
use App\Modules\User\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UserRepository implements IUserRepository
{
    private ReportUserActionTime $reportUserActionTime;

    public function __construct()
    {
        $this->reportUserActionTime = app(ReportUserActionTime::class);
    }

    public function login(string $email, string $password): bool
    {
        $credentials = ['email' => $email, 'password' => $password];
        return auth()->attempt($credentials);
    }

    public function getUserByEmail(string $email): EUser
    {
        return User::with('branchOffice', 'roles')->where('email', $email)->first()->toDomainEntity();
    }

    public function getUserById(int $userId): EUser
    {
        return User::with('visitPurposes', 'branchOffice', 'userStatus', 'roles')
            ->where('id', $userId)
            ->first()
            ->toDomainEntity();
    }

    public function updateSettingsData(array $newWorkData): EUser
    {
        $user = User::find($newWorkData['userId']);
        $user->visitPurposes()->sync($newWorkData['visitPurposesIds']);
        $branchOffice = BranchOffice::find($newWorkData['branchOfficeId']);
        $fullDeskNumber = $newWorkData['deskNumber'] . $branchOffice->initial;

         $userIsUpdated = $user->update([
            'branch_office_id' => $newWorkData['branchOfficeId'],
            'desk_number' => $fullDeskNumber,
        ]);

        if($newWorkData['userIdWithSameDeskNumber']) {
            $existedUser = User::find($newWorkData['userIdWithSameDeskNumber']);
            $existedUser->update([
                'desk_number' => null
            ]);
        }

        return User::with('visitPurposes', 'branchOffice', 'userStatus', 'roles')
            ->find($user->id)
            ->toDomainEntity();
    }

    public function getAllTicketsFilteredByBranchOfficeId(int $branchOfficeId): object
    {
        $tickets = Ticket::select('completed_at','invited_at')->where('branch_office_id', $branchOfficeId)->get();
        return $tickets->map(function ($ticket) {
            return $ticket->toDomainEntity();
        });
    }

    public function getAllTicketsFilteredByUserIdVisitPurposesIds(int $userId, array $visitPurposesIds): object
    {
        $tickets = Ticket::select('completed_at','invited_at')
            ->where('user_id', $userId)
            ->whereIn('visit_purpose_id', $visitPurposesIds)
            ->get();

        return $tickets->map(function ($ticket) {
            return $ticket->toDomainEntity();
        });
    }

    public function getUsersMaxServiceTime(int $branchOfficeId): string|null
    {
        return DB::table('tickets')
            ->selectRaw('max(timediff(completed_at, invited_at)) as usersMaxServiceTime')
            ->where('branch_office_id', $branchOfficeId)
            ->first()->usersMaxServiceTime;
    }

    public function  getCustomersAmountUserServed(int $userId): string
    {
        return Ticket::whereDate('completed_at', '=',date('Y-m-d'))
            ->where('user_id', $userId)
            ->get()
            ->count();
    }

    public function getDeskNumberExistingCount(string $deskNumber): int
    {
        return User::where('desk_number', $deskNumber)->get()->count();
    }

    public function getUsersList(): object
    {
        return User::with('roles')->get()->map(function ($user) {
            return $user->toDomainEntity()->jsonSerialize();
        });
    }

    public function getUserFilteredByDeskNumber(string $deskNumber): EUser
    {
        return User::with('visitPurposes', 'branchOffice')
            ->where('desk_number', $deskNumber)
            ->first()->toDomainEntity();
    }

    public function getUserDataFilteredById (int $id): array
    {
        return User::with('visitPurposes', 'branchOffice', 'userStatus', 'roles')->find($id)->toDomainEntity()->jsonSerialize();
    }

    public function updateStatus(int $userId, int $statusId): bool
    {
        $user = User::find($userId);
        return $user->update(['status_id' => $statusId, 'updated_at' => Carbon::now()]);
    }

    public function updateUserActionTimeReport(EUser $userDataBeforeBeingUpdated): bool
    {
        $dateFrom = Carbon::today()->toDateString().' 00:00:00';
        $dateTo = Carbon::today()->toDateString() .' 23:59:59';
        $userId = $userDataBeforeBeingUpdated->getId();
        $userDataAfterBeingUpdated = User::find($userId)->toDomainEntity();
        $report = $this->reportUserActionTime
            ->where("created_at", '>=', $dateFrom)
            ->where("created_at", '<=' , $dateTo)
            ->where("user_id", $userId)
            ->first();
        $periodOfTimeWhenStatusUpdated = gmdate(
            "H:i:s",
            strtotime($userDataAfterBeingUpdated->getUpdatedAt()) - strtotime($userDataBeforeBeingUpdated->getUpdatedAt()),
        );

        if($report) {
            $statusColumnName = $userDataBeforeBeingUpdated->getStatusColumnName();

            return $report->update([
                 $statusColumnName => date(
                    "H:i:s",
                     strtotime($report->{$statusColumnName}) + strtotime($periodOfTimeWhenStatusUpdated) - strtotime('00:00:00'),
                )
            ]);

        }

        $this->reportUserActionTime->create([
            'user_id' => $userId
        ]);

        return true;
    }

    public function isUserHasClient(int $userId) {
        $dateFrom = Carbon::today()->toDateString().' 00:00:00';
        $dateTo = Carbon::today()->toDateString() .' 23:59:59';
        $ticket = Ticket::where('user_id', $userId)
            ->where('completed_at', null)
            ->where('canceled_at', null)
            ->where('created_at', '>=', $dateFrom)
            ->where('created_at', '<=', $dateTo)
            ->first();

        return $ticket ? true : false;
    }
}
