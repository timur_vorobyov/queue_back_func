<?php


namespace App\Modules\User\Person\Events;


use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserDeskNumberEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private array $userData;

    public function __construct(array $userData) {
        $this->userData = $userData;

    }

    public function broadcastAs()
    {
        return "user-data";
    }

    public function broadcastWith()
    {
        return [
            'userData' => $this->userData,
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new Channel("user-data.{$this->userData['id']}"),
        ];
    }
}
