<?php


namespace App\Modules\User\Person\Events;


use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserStatisticsDataEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private string $usersAverageServiceTime;
    private string $userAverageServiceTime;
    private string $customersAmountUserServed;
    private string $usersMaxServiceTime;
    private int $userId;

    public function __construct(
        int $userId,
        string $usersAverageServiceTime,
        string $userAverageServiceTime,
        string $customersAmountUserServed,
        string $usersMaxServiceTime
    ) {
        $this->userId = $userId;
        $this->usersAverageServiceTime = $usersAverageServiceTime;
        $this->userAverageServiceTime = $userAverageServiceTime;
        $this->customersAmountUserServed = $customersAmountUserServed;
        $this->usersMaxServiceTime = $usersMaxServiceTime;
    }

    public function broadcastAs()
    {
        return "statistics-data";
    }

    public function broadcastWith()
    {
        return [
            "userStatisticsData" => [
                "usersAverageServiceTime" => $this->usersAverageServiceTime,
                "userAverageServiceTime" => $this->userAverageServiceTime,
                "customersAmountUserServed" => $this->customersAmountUserServed,
                "usersMaxServiceTime" => $this->usersMaxServiceTime,
            ],
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel("users.{$this->userId}.statistics");
    }
}
