<?php


namespace App\Modules\User\Person\Service;


use App\Modules\User\Person\Interfaces\IUserRepository;

class StatisticsRecipientService
{
    protected IUserRepository $userRepository;

    public function __construct(IUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getUsersAverageServiceTime(int $branchOfficeId): string
    {
        $tickets = $this->userRepository->getAllTicketsFilteredByBranchOfficeId($branchOfficeId);
        $usersTicketAmount = 0;
        $usersServiceTime = 0;

        foreach ($tickets as $ticket) {

            if(!$ticket->getCompletedAt() || !$ticket->getInvitedAt()) {
                continue;
            }

            $usersServiceTime += strtotime($ticket->getCompletedAt()) - strtotime($ticket->getInvitedAt());
            $usersTicketAmount++;
        }

        if ($usersTicketAmount == 0) {
            return '0';
        }

        return 1 * date("i", $usersServiceTime/$usersTicketAmount);
    }

    public function getUserAverageServiceTime(int $userId, array $visitPurposeId): string
    {
        $tickets = $this->userRepository->getAllTicketsFilteredByUserIdVisitPurposesIds( $userId, $visitPurposeId);
        $userTicketAmount = 0;
        $userServiceTime = 0;

        foreach ($tickets as $ticket) {

            if(!$ticket->getCompletedAt() || !$ticket->getInvitedAt())
            {
                continue;
            }

            $userServiceTime += strtotime($ticket->getCompletedAt()) - strtotime($ticket->getInvitedAt());
            $userTicketAmount++;
        }

        if ($userTicketAmount == 0) {
            return '0';
        }

        return 1 * date("i", $userServiceTime/$userTicketAmount);
    }

    public function  getUsersMaxServiceTime(int $branchOfficeId):string
    {
        return 1 * date("i", strtotime($this->userRepository->getUsersMaxServiceTime($branchOfficeId)));
    }

    public function  getCustomersAmountUserServed(int $userId):string
    {
        return $this->userRepository->getCustomersAmountUserServed($userId);
    }

    public function getUsersList(): object
    {
        return $this->userRepository->getUsersList();
    }
}
