<?php

namespace App\Modules\User;

use App\Interfaces\IJsonSerializable;
use App\Modules\BranchOffice\EBranchOffice;
use App\Modules\Role\Role;

class EUser implements IJsonSerializable
{
    private ?int $id;
    private int $isAllowedHaveSameDeskNumber;
    private ?int $branchOfficeId;
    private ?object $visitPurposes;
    private string $firstName;
    private ?string $lastName;
    private string $email;
    private ?string $deskNumber;
    private bool $isActive;
    private string $fullName;
    private ?EBranchOffice $branchOffice;
    private string $createdAt;
    private string $updatedAt;
    private ?Role $role;
    private int $userStatusId;
    private string $userStatusName;

    public function __construct(
        ?int $id,
        ?int $branchOfficeId,
        ?object $visitPurposes,
        string $firstName,
        ?string $lastName,
        string $email,
        ?string $deskNumber,
        bool $isActive,
        ?EBranchOffice $branchOffice,
        string $createdAt,
        string $updatedAt,
        ?Role $role,
        bool $isAllowedHaveSameDeskNumber,
        int $userStatusId,
        string $userStatusName
    ) {
        $this->id = $id;
        $this->isAllowedHaveSameDeskNumber = $isAllowedHaveSameDeskNumber;
        $this->branchOfficeId = $branchOfficeId;
        $this->visitPurposes = $visitPurposes;
        $this->firstName = $firstName;
        $this->lastName = $lastName ? $lastName : '';
        $this->email = $email;
        $this->deskNumber = $deskNumber;
        $this->isActive = $isActive;
        $this->fullName = $this->firstName . " " . $this->lastName;
        $this->branchOffice = $branchOffice;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->role = $role;
        $this->userStatusId = $userStatusId;
        $this->userStatusName = $userStatusName;
    }

    public function isAllowedHaveSameDeskNumber(): bool
    {
        return $this->isAllowedHaveSameDeskNumber;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEBranchOffice(): ?EBranchOffice
    {
        return $this->branchOffice;
    }

    public function getBranchOfficeId(): ?int
    {
        return $this->branchOfficeId;
    }

    public function getDeskNumber(): ?string
    {
        return $this->deskNumber;
    }

    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    public function getStatusColumnName(): string
    {
        return str_replace(' ', '_', $this->userStatusName);
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "branchOfficeId" => $this->branchOfficeId,
            "branchOfficeFullName" =>  $this->branchOffice ? $this->getBranchOfficeFullName() : "",
            "timer" => $this->branchOffice ? $this->branchOffice->getTimer() : 5000,
            "townId" => $this->branchOffice ? $this->branchOffice->getTown()->getId() : 0,
            "firstName" => $this->firstName,
            "lastName" => $this->lastName,
            "email" => $this->email,
            "deskNumber" =>  $this->deskNumber
                ? preg_replace('/\D/', '', $this->deskNumber)
                : "",
            "isActive" => $this->isActive,
            "visitPurposesIds" => $this->visitPurposes->pluck('id'),
            "visitPurposesNames" => $this->visitPurposes->map(function ($visitPurpose) {
                return $visitPurpose->toDomainEntity()->getName();
            }),
            "fullName" => $this->fullName,
            "roleName" => $this->role ? $this->role->toDomainEntity()->getName() : '',
            "userStatusId" => $this->userStatusId,
            "userStatusName" => $this->userStatusName,
        ];
    }

    public function superAdminJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "branchOffice" => [
                "id"=> $this->branchOffice->getId(),
                "name"=> $this->getBranchOfficeFullName(),
                "townId" => $this->branchOffice->getTown()->getId()
            ] ,
            "email" => $this->email,
            "firstName" => $this->firstName,
            "lastName" => $this->lastName,
            "isActive" => $this->isActive,
            "createdAt" => date("Y/m/d", strtotime($this->createdAt)),
            "role" => $this->role,
            "visitPurposesIds" => $this->visitPurposes ? $this->visitPurposes->map(function ($visitPurpose) {
                return $visitPurpose->toDomainEntity()->getId();
            }) : null,
            "deskNumber" => $this->deskNumber,
            "isAllowedHaveSameDeskNumber"=> $this->isAllowedHaveSameDeskNumber,
        ];
    }

    public function hallAdminJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "email" => $this->email,
            "firstName" => $this->firstName,
            "lastName" => $this->lastName,
            "isActive" => $this->isActive,
            "fullName" => $this->fullName,
            "branchOfficeName" =>  $this->branchOffice ? $this->getBranchOfficeFullName() : '',
            "createdAt" => date("Y/m/d", strtotime($this->createdAt)),
            "role" => $this->role,
            "deskNumber" => $this->deskNumber,
        ];
    }

    private function getBranchOfficeFullName(): string
    {
        return $this->branchOffice->getTown()->getName() .
            " " .
            $this->branchOffice->getName();
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function adminJsonSerialize(): array
    {
        // TODO: Implement adminJsonSerialize() method.
    }
}
