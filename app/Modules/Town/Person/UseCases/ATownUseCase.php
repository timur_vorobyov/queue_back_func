<?php

namespace App\Modules\Town\Person\UseCases;

use App\Modules\Town\Person\Interfaces\ITownRepository;

abstract class ATownUseCase
{
    protected ITownRepository $townRepository;

    public function __construct(ITownRepository $townRepository)
    {
        $this->townRepository = $townRepository;
    }
}
