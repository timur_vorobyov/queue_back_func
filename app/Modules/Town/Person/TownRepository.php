<?php

namespace App\Modules\Town\Person;

use App\Modules\Town\Person\Interfaces\ITownRepository;
use App\Modules\Town\Town;

class TownRepository implements ITownRepository
{
    public function getAllTowns(): array
    {
        return Town::all()->toArray();
    }
}
