<?php

namespace App\Modules\Town;

use App\Interfaces\IJsonSerializable;

class ETown implements IJsonSerializable
{
    private int $id;
    private string $name;

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function superAdminJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
        ];
    }

    public function adminJsonSerialize(): array
    {
        // TODO: Implement adminJsonSerialize() method.
    }

    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
        ];
    }
}
