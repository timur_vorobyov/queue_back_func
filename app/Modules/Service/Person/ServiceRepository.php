<?php

namespace App\Modules\Service\Person;

use App\Modules\Service\Person\Interfaces\IServiceRepository;
use App\Modules\Service\Service;

class ServiceRepository implements IServiceRepository
{

    public function getServiceListFilteredByVisitPurposeId(int $visitPurposeId): object
    {
        $services = Service
            ::with("visitPurposes")
            ->whereHas("visitPurposes", function ($q) use ($visitPurposeId) {
                $q->whereIn('visit_purpose_id', [$visitPurposeId]);
            })
            ->where("is_active", true)
            ->get();
        return $services->map(function ($service) {
            return $service->toDomainEntity()->jsonSerialize();
        });
    }
}
