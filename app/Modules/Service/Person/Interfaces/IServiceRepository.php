<?php

namespace App\Modules\Service\Person\Interfaces;

interface IServiceRepository
{
    public function getServiceListFilteredByVisitPurposeId(int $visitPurposeId): object;
}
