<?php

namespace App\Modules\Permission;

use App\Interfaces\IJsonSerializable;

class EPermission implements IJsonSerializable
{
    private int $id;
    private string $name;
    private string $guardName;

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->guardName = "web";
    }

    public function superAdminJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "guardName" => $this->guardName,
        ];
    }

    public function adminJsonSerialize(): array
    {
        // TODO: Implement adminJsonSerialize() method.
    }

    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
        ];
    }
}
