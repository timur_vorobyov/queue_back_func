<?php

namespace App\Modules\Role;

use App\Interfaces\IJsonSerializable;

class ERole implements IJsonSerializable
{
    private int $id;
    private string $name;
    private string $guardName;
    private ?object $permissions;

    public function __construct(int $id, string $name, ?object $permissions)
    {
        $this->id = $id;
        $this->name = $name;
        $this->guardName = "web";
        $this->permissions = $permissions;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function superAdminJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "permissions" => $this->permissions->map(function ($permission) {
                return $permission->toDomainEntity()->superAdminJsonSerialize();
            }),
        ];
    }

    public function adminJsonSerialize(): array
    {
        // TODO: Implement adminJsonSerialize() method.
    }

    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
        ];
    }
}
