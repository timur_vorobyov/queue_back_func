<?php

namespace App\Modules\Role;

use App\Modules\Permission\Permission;
use Database\Factories\RoleFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Role extends \Spatie\Permission\Models\Role
{
    use HasFactory;

    protected $fillable = ["name", "guard_name"];

    protected static function newFactory()
    {
        return RoleFactory::new();
    }

    public function toDomainEntity(): ERole
    {
        return new ERole(
            $this->id,
            $this->name,
            $this->permissions
        );
    }
}
