<?php

namespace App\Modules\SubVisitPurpose;


use App\Interfaces\IJsonSerializable;
use App\Modules\VisitPurpose\EVisitPurpose;

class ESubVisitPurpose implements IJsonSerializable
{
    private int $id;
    private string $name;
    private ?EVisitPurpose $visitPurpose;

    public function __construct(
        int $id,
        string $name,
        ?EVisitPurpose $visitPurpose
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->visitPurpose = $visitPurpose;
    }


    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "visitPurpose" => $this->visitPurpose
                ? $this->visitPurpose->jsonSerialize()
                : null,
        ];
    }

    public function superAdminJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "visitPurpose" => $this->visitPurpose
                ? $this->visitPurpose->jsonSerialize()
                : null,
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }


    public function adminJsonSerialize(): array
    {
        // TODO: Implement adminJsonSerialize() method.
    }
}
