<?php

namespace App\Modules\SubVisitPurpose\Person\Interfaces;

interface ISubVisitPurposeRepository
{
    public function getSubVisitPurposesListFilteredByVisitPurposeId(int $visitPurposeId): object;
}
