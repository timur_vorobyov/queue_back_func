<?php


namespace App\Modules\SubVisitPurpose\Person;


use App\Modules\SubVisitPurpose\Person\Interfaces\ISubVisitPurposeRepository;
use App\Modules\SubVisitPurpose\SubVisitPurpose;

class SubVisitPurposeRepository  implements ISubVisitPurposeRepository
{

    public function getSubVisitPurposesListFilteredByVisitPurposeId(int $visitPurposeId): object
    {
        $subVisitPurposes = SubVisitPurpose
            ::with("visitPurpose")
            ->where('visit_purpose_id', $visitPurposeId)
            ->get();
        return $subVisitPurposes->map(function ($subVisitPurpose) {
            return $subVisitPurpose->toDomainEntity()->jsonSerialize();
        });
    }
}
