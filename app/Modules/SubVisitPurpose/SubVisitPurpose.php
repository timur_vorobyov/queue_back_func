<?php

namespace App\Modules\SubVisitPurpose;

use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubVisitPurpose extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function visitPurpose()
    {
        return $this->belongsTo(VisitPurpose::class);
    }

    public function toDomainEntity(): ESubVisitPurpose
    {
        $relations = $this->getRelations();

        return new ESubVisitPurpose(
            $this->id,
            $this->name,
            key_exists('visitPurpose', $relations)
            && !is_null($this->visitPurpose)
                ? $this->visitPurpose->toDomainEntity()
                : null,
        );
    }
}
