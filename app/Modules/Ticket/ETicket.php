<?php

namespace App\Modules\Ticket;

use App\Modules\BranchOffice\EBranchOffice;
use App\Modules\Service\EService;
use App\Modules\SubVisitPurpose\ESubVisitPurpose;
use App\Modules\User\EUser;
use App\Modules\VisitPurpose\EVisitPurpose;

class ETicket
{
    private ?int $id;
    private ?int $crmCustomerId;
    private ?int $visitPurposeId;
    private ?string $customerPhoneNumber;
    private ?int $branchOfficeId;
    private ?int $ticketNumber;
    private ?int $userId;
    private ?int $statusId;
    private ?int $serviceId;
    private ?string $invitedAt;
    private ?string $servedAt;
    private ?string $completedAt;
    private ?string $canceledAt;
    private ?string $createdAt;
    private ?EVisitPurpose $visitPurpose;
    private ?EUser $user;
    private ?EBranchOffice $branchOffice;
    private ?EService $service;
    private ?ESubVisitPurpose $subVisitPurpose;
    private ?bool $isSalomCache;

    public function __construct(
        ?int $id,
        ?int $crmCustomerId,
        ?int $visitPurposeId,
        ?string $customerPhoneNumber,
        ?int $branchOfficeId,
        ?int $ticketNumber,
        ?int $userId = null,
        ?int $statusId = 1,
        ?int $serviceId = null,
        ?string $invitedAt = null,
        ?string $servedAt = null,
        ?string $completedAt = null,
        ?string $canceledAt = null,
        ?string $createdAt = null,
        ?EVisitPurpose $visitPurpose = null,
        ?EBranchOffice $branchOffice = null,
        ?EUser $user = null,
        ?EService $service = null,
        ?ESubVisitPurpose $subVisitPurpose = null,
        ?bool $isSalomCache = false
    ) {
        $this->id = $id;
        $this->crmCustomerId = $crmCustomerId;
        $this->visitPurposeId = $visitPurposeId;
        $this->customerPhoneNumber = $customerPhoneNumber;
        $this->branchOfficeId = $branchOfficeId;
        $this->ticketNumber = $ticketNumber;
        $this->userId = $userId;
        $this->statusId = $statusId;
        $this->serviceId = $serviceId;
        $this->invitedAt = $invitedAt;
        $this->servedAt = $servedAt;
        $this->servedAt = $servedAt;
        $this->completedAt = $completedAt;
        $this->canceledAt = $canceledAt;
        $this->createdAt = $createdAt;
        $this->visitPurpose = $visitPurpose;
        $this->branchOffice = $branchOffice;
        $this->user = $user;
        $this->service = $service;
        $this->subVisitPurpose = $subVisitPurpose;
        $this->isSalomCache = $isSalomCache;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEVisitPurpose(): ?EVisitPurpose
    {
        return $this->visitPurpose;
    }

    public function getESubVisitPurpose(): ?ESubVisitPurpose
    {
        return $this->subVisitPurpose;
    }

    public function getUser(): ?EUser
    {
        return $this->user;
    }

    public function getEBranchOffice(): ?EBranchOffice
    {
        return $this->branchOffice;
    }

    public function getEService(): ?EService
    {
        return $this->service;
    }

    public function getVisitPurposeId(): int
    {
        return $this->visitPurposeId;
    }

    public function getCustomerPhoneNumber(): ?string
    {
        return $this->customerPhoneNumber;
    }

    public function getBranchOfficeId(): int
    {
        return $this->branchOfficeId;
    }

    public function getTicketNumber(): int
    {
        return $this->ticketNumber;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function getCompletedAt(): ?string
    {
        return $this->completedAt;
    }

    public function getInvitedAt(): ?string
    {
        return $this->invitedAt;
    }

    public function getCrmCustomerId(): ?int
    {
        return $this->crmCustomerId;
    }

    public function getIsSalomCache(): ?bool
    {
        return $this->isSalomCache;
    }

    //Data transfer object
    public function jsonSerialize(): array
    {

        return [
            "id" => $this->id,
            "crmCustomerId" => $this->crmCustomerId,
            "customerPhoneNumber" => $this->customerPhoneNumber,
            "createdAt" => date("H:i:s", strtotime($this->createdAt)),
            "pendingTime" => gmdate(
                "H:i:s",
                strtotime($this->invitedAt) - strtotime($this->createdAt),
            ),
            "servedAt" => $this->servedAt,
            "canceledAt" => $this->canceledAt,
            "completedAt" => $this->completedAt,
            "invitedAt" => $this->invitedAt,
            "visitPurposeName" => $this->visitPurpose ? $this->visitPurpose->getName() : '',
            "visitPurposeId" => $this->visitPurpose ? $this->visitPurpose->getId() : 0,
            "isVisitPurposePrioratised" => $this->visitPurpose ? $this->visitPurpose->getIsPrioratised() : false,
            "ticketFullNumber" => $this->branchOffice ? $this->getFullTicketNumberWithDash() : '',
            "isSalomCache" => $this->isSalomCache,
            "subVisitPurpose" => $this->subVisitPurpose ? $this->subVisitPurpose->jsonSerialize(): null
        ];
    }

    //Data transfer object
    public function jsonSerializeShortTicket(): array
    {
        return [
            "id" => $this->id,
            "deskNumber" => $this->user
                ? preg_replace("/\D/", "", $this->user->getDeskNumber())
                : null,
            "ticketFullNumber" => $this->branchOffice ? $this->getFullTicketNumberWithoutDash() : '',
            "isVisitPurposePrioratised" => $this->visitPurpose ? $this->visitPurpose->getIsPrioratised() : false,
            "isSalomCache" => $this->isSalomCache,
            "isCashRegister" =>  $this->visitPurpose->getIsCashRegister()
        ];
    }


    //Data transfer object
    public function exportJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "crmCustomerId" => $this->crmCustomerId,
            "customerPhoneNumber" => $this->customerPhoneNumber,
            "createdAt" => date("H:i:s", strtotime($this->createdAt)),
            "pendingTime" => gmdate(
                "H:i:s",
                strtotime($this->invitedAt) - strtotime($this->createdAt),
            ),
            "servedAt" => $this->servedAt,
            "canceledAt" => $this->canceledAt,
            "completedAt" => $this->completedAt,
            "invitedAt" => $this->invitedAt,
            "visitPurposeName" => $this->visitPurpose ? $this->visitPurpose->getName() : '',
            "visitPurposeId" => $this->visitPurpose ? $this->visitPurpose->getId() : 0,
            "ticketFullNumber" => $this->branchOffice ? $this->getFullTicketNumberWithDash() : '',
            "isSalomCache" => $this->isSalomCache,
        ];
    }


    public function getFullTicketNumberWithoutDash(): string
    {
        return "{$this->branchOffice->getInitial()}{$this->visitPurpose->getSeries()}{$this->getTicketNumber()}";
    }

    public function getFullTicketNumberWithDash(): string
    {
        return "{$this->branchOffice->getInitial()} - {$this->visitPurpose->getSeries()} - {$this->getTicketNumber()}";
    }

    public function jsonSerializeCreatedTicket (): array {
        return [
            "ticketFullNumber" => $this->branchOffice ? $this->getFullTicketNumberWithoutDash() : '',
        ];
    }

    public function getBranchOfficeFullName(): string
    {
        return $this->branchOffice->getTown()->getName() .
            " " .
            $this->branchOffice->getName();
    }

    //Data transfer object
    public function adminJsonSerialize(): array {
        return [
            "id" => $this->id,
            "customerPhoneNumber" => $this->customerPhoneNumber,
            "createdAt" => date("H:i:s", strtotime($this->createdAt)),
            "visitPurposeName" => $this->visitPurpose ? $this->visitPurpose->getName() : '',
            "ticketFullNumber" => $this->branchOffice ? $this->getFullTicketNumberWithDash() : '',
            "isSalomCache" => $this->isSalomCache,
        ];
    }
}
