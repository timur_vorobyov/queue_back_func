<?php

namespace App\Modules\Ticket\Person\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface ITicketRepository
{
    public function saveTicket(array $createTicketData): object;
    public function getShortTicketsList(
        int $branchOfficeId,
        string $statusName,
        array $visitPurposeIds
    ): object;
    public function getTicket(
        int $branchOfficeId,
        string $statusName,
        array $visitPurposesIds,
        int $userId
    ): array | null;
    public function getFullTicketsList(
        int $branchOfficeId,
        string $statusName,
        array $visitPurposesIds
    ): Collection;
    public function isUserDeskNumberNull(int $userId): bool;
    public function getUserVisitPurposesCount(int $userId): int;
    public function updateTicket(array $dataToUpdate): int;
    public function getTicketFilteredById(
        int $ticketId
    );
    public function associateBranches(array $params);
}
