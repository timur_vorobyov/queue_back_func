<?php


namespace App\Modules\Ticket\Person\Jobs;


use App\Services\SmsSurveyService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SmsSurveyServiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $customerPhoneNumber;
    private string $visitPurposeName;
    private int $userId;
    private string $useFullName;
    private string $townName;
    private string $branchOfficeName;
    private string $completedAt;

    public function __construct(
        string $customerPhoneNumber,
        string $visitPurposeName,
        string $userId,
        string $useFullName,
        string $townName,
        string $branchOfficeName,
        string $completedAt
    )
    {
        $this->customerPhoneNumber = $customerPhoneNumber;
        $this->visitPurposeName = $visitPurposeName;
        $this->userId = $userId;
        $this->useFullName = $useFullName;
        $this->townName = $townName;
        $this->branchOfficeName = $branchOfficeName;
        $this->completedAt = $completedAt;
    }

    public function handle(SmsSurveyService $smsSurveyService)
    {
        $smsSurveyService->send(
            $this->customerPhoneNumber,
            $this->visitPurposeName,
            $this->userId,
            $this->useFullName,
            $this->townName,
            $this->branchOfficeName,
            $this->completedAt
        );
    }
}
