<?php


namespace App\Modules\Ticket\Person\Jobs;


use App\Modules\Ticket\ETicket;
use App\Modules\Ticket\Person\Events\TicketAcceptEvent;
use App\Modules\Ticket\Person\Events\TicketPendingEvent;
use App\Modules\Ticket\Person\TicketRepository;
use App\Modules\Ticket\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateTicketToAcceptStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private  Ticket $ticket;
    private  ETicket $pureTicketObject;
    private  array $dataToUpdateTicket;
    private TicketRepository $ticketRepository;

    public function __construct(Ticket $ticket, array $dataToUpdateTicket)
    {
        $this->ticket = $ticket;
        $this->dataToUpdateTicket = $dataToUpdateTicket;
        $this->ticketRepository = app(TicketRepository::class);
        $this->pureTicketObject = $ticket->toDomainEntity();
    }


    public function handle()
    {
        $currentTicket = $this->elementBusyChecker();
        $dataToUpdateNewTicket = $this->dataToUpdateTicket;
        $dataToUpdateNewTicket['ticketId'] = $currentTicket->id;
       $this->ticketRepository->updateTicket(
           $dataToUpdateNewTicket,
        );

        $ticket = $this->ticketRepository->getTicketFilteredById(
            $currentTicket->id
        );
        $this->runEvents($ticket->toDomainEntity());
    }

    private function elementBusyChecker() {
        $currentTicket = null;

        if($this->ticketRepository->isIsTicketServing($this->ticket->id)) {
            $tickets = $this->ticketRepository->getFullTicketsList(
                $this->pureTicketObject->getBranchOfficeId(),
                'new',
                [$this->pureTicketObject->getVisitPurposeId()]
            );

            $currentTicket = $tickets->first();
        } else {
            $currentTicket = $this->ticket;
        }

        return $currentTicket;
    }

    private function runEvents(ETicket $pureTicketObject) {
        event(new TicketPendingEvent($pureTicketObject, $pureTicketObject->getVisitPurposeId(), "delete"));
        event(
            new TicketAcceptEvent(
                $pureTicketObject,
                $pureTicketObject->getUserId(),
                "add",
            ),
        );
        event(new TicketAcceptEvent($pureTicketObject, "all", "add"));
        CallSenderJob::dispatch($pureTicketObject->getId())->delay(now()->addSeconds(60))->onQueue('calls');
        return "You have accepted client";
    }
}
