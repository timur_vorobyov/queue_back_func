<?php


namespace App\Modules\Ticket\Person\Jobs;


use App\Modules\Reports\ReportTicket\ReportTicket;
use App\Modules\Ticket\ETicket;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ReportTicketJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private ETicket $pureSavedTicket;

    public function __construct($pureSavedTicket)
    {
        $this->pureSavedTicket = $pureSavedTicket;
    }

    public function handle () {
        ReportTicket::create([
            "ticket_id" => $this->pureSavedTicket->getId(),
            "ticket_full_number" => $this->pureSavedTicket->getFullTicketNumberWithoutDash(),
            "visit_purpose_name" =>$this->pureSavedTicket->getEVisitPurpose()->getName(),
            "service_name" => $this->pureSavedTicket->getEService() ? $this->pureSavedTicket->getEService()->getName() : '',
            "customer_phone_number" => $this->pureSavedTicket->getCustomerPhoneNumber(),
            "date_created_at" => date("Y-m-d", strtotime($this->pureSavedTicket->getCreatedAt())),
            "time_created_at" => date("H:i:s", strtotime($this->pureSavedTicket->getCreatedAt())),
            "branch_office_name" => $this->pureSavedTicket->getBranchOfficeFullName(),
            "customer_service_time" => "Клиент не подошёл",
            "customer_pending_time" => "Клиента не пригласили"
        ]);
    }
}
