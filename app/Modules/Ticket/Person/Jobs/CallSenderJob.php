<?php

namespace App\Modules\Ticket\Person\Jobs;

use App\Modules\Status\Status;
use App\Modules\Ticket\Ticket;
use App\Services\CallSenderService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CallSenderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $ticketId;

    public function __construct($ticketId)
    {
        $this->ticketId = $ticketId;
    }

    public function handle(Status $status, Ticket $ticket)
    {
        $status = $status::where('name', 'invited')->first();
        $ticket = $ticket::where('status_id', $status->id)->find($this->ticketId);

        if ($ticket) {
            if ($ticket->toDomainEntity()->getCustomerPhoneNumber()) {
                (app(CallSenderService::class))->send($ticket->toDomainEntity()->getCustomerPhoneNumber());
            }
        }
    }
}
