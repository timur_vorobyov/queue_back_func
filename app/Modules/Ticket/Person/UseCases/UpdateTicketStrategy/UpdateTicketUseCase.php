<?php

namespace App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy;

use App\Modules\Ticket\Person\Events\TicketPendingEvent;
use App\Modules\Ticket\Person\TicketRepository;
use App\Modules\Ticket\Person\UseCases\ATicketUseCase;
use App\Requests\Person\ARequest;

class UpdateTicketUseCase extends ATicketUseCase
{
    protected IUpdateTicketStrategy $useCaseStrategy;

    public function __construct(IUpdateTicketStrategy $useCaseStrategy)
    {
        parent::__construct(app(TicketRepository::class));
        $this->useCaseStrategy = $useCaseStrategy;
    }

    public function perform(ARequest $request): string
    {
        $request->validate();

        if (
            $this->ticketRepository->getUserVisitPurposesCount(
                $request->getUserId(),
            ) == 0
        ) {
            return "The visit goal wasn't chosen";
        }

        if (
            $this->ticketRepository->isUserDeskNumberNull($request->getUserId())
        ) {
            return "The number of desk wasn't chosen";
        }

        $this->useCaseStrategy->runEvents($request->getDataToUpdateTicket());

        return "TicketController's state haven't changed";
    }
}
