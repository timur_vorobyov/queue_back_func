<?php

namespace App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy;

interface IUpdateTicketStrategy
{
    public function runEvents(array $dataToUpdateTicket): string;
}
