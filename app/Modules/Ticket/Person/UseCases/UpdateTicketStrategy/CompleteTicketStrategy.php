<?php

namespace App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy;

use App\Modules\Ticket\Person\Events\TicketServiceEvent;
use App\Modules\Ticket\Person\Jobs\SmsSurveyServiceJob;
use App\Modules\Ticket\Person\TicketRepository;

class CompleteTicketStrategy implements IUpdateTicketStrategy
{
    private TicketRepository $ticketRepository;
    public function __construct()
    {
        $this->ticketRepository = app(TicketRepository::class);
    }

    public function runEvents(array $dataToUpdateTicket): string
    {
        $ticketId =  $this->ticketRepository->updateTicket(
            $dataToUpdateTicket
        );

        $ticket = $this->ticketRepository->getTicketFilteredById(
            $ticketId
        );

        $pureTicketObject = $ticket->toDomainEntity();
        event(new TicketServiceEvent($pureTicketObject, "delete"));
        SmsSurveyServiceJob::dispatch(
            $pureTicketObject->getCustomerPhoneNumber(),
            $pureTicketObject->getEVisitPurpose()->getName(),
            $pureTicketObject->getUser()->getId(),
            $pureTicketObject->getUser()->getFullName(),
            $pureTicketObject->getEBranchOffice()->getTown()->getName(),
            $pureTicketObject->getEBranchOffice()->getName(),
            $pureTicketObject->getCompletedAt()
        )->onQueue('sendSmsSurvey');

        return "Service is completed";
    }
}
