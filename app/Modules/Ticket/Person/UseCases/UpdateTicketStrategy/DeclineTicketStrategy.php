<?php

namespace App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy;


use App\Modules\Ticket\Person\Events\TicketAcceptEvent;
use App\Modules\Ticket\Person\TicketRepository;
use App\Modules\Ticket\Ticket;

class DeclineTicketStrategy implements IUpdateTicketStrategy
{
    private TicketRepository $ticketRepository;
    public function __construct()
    {
        $this->ticketRepository = app(TicketRepository::class);
    }

    public function runEvents(array $dataToUpdateTicket): string
    {
        $ticketId =  $this->ticketRepository->updateTicket(
            $dataToUpdateTicket
        );

        $ticket = $this->ticketRepository->getTicketFilteredById(
            $ticketId
        );

        $pureTicketObject = $ticket->toDomainEntity();
        event(
            new TicketAcceptEvent(
                $pureTicketObject,
                $pureTicketObject->getUserId(),
                "delete",
            ),
        );
        event(new TicketAcceptEvent($pureTicketObject, "all", "delete"));

        return "Client is declined";
    }
}
