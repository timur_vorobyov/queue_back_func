<?php

namespace App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy;

use App\Modules\Ticket\Person\Jobs\UpdateTicketToAcceptStatusJob;
use App\Modules\Ticket\Person\TicketRepository;
use App\Modules\Ticket\Ticket;


class AcceptTicketStrategy implements IUpdateTicketStrategy
{
    private TicketRepository $ticketRepository;
    public function __construct()
    {
        $this->ticketRepository = app(TicketRepository::class);
    }

    public function runEvents(array $dataToUpdateTicket): string
    {

        $ticket = $this->ticketRepository->getTicketFilteredById(
            $dataToUpdateTicket['ticketId']
        );

        UpdateTicketToAcceptStatusJob::dispatch($ticket, $dataToUpdateTicket)->onQueue('acceptTickets');

        return "TICKET_ACCEPTED";
    }
}
