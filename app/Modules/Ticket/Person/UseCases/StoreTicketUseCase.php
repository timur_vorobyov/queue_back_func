<?php

namespace App\Modules\Ticket\Person\UseCases;

use App\Modules\Ticket\Person\Events\TicketPendingEvent;
use App\Modules\Ticket\Person\Jobs\SmsSenderJob;
use App\Requests\Person\ARequest;
use App\Services\CrmService;

class StoreTicketUseCase extends ATicketUseCase
{
    public function perform(ARequest $request): array
    {
        $request->validate();

        $savedTicket = $this->ticketRepository->saveTicket(
            $request->getDataToStoreTicket(),
        );
        $pureTicketObject = $savedTicket->toDomainEntity();

        if ($pureTicketObject->getCustomerPhoneNumber()) {
            $customerSearchService = app(CrmService::class);
            $customerData = $customerSearchService->searchFromCRM($pureTicketObject->getCustomerPhoneNumber());

            if(!isset($customerData->hasClient)) {
                $savedTicket->update([
                    'crm_customer_id' => null
                ]);
            } else {
                $savedTicket->update([
                    'crm_customer_id' =>
                        $customerData->hasClient ?
                        $customerData->clientId :
                        null
                ]);
            }

            $message =
                "Навбати шумо таҳти рақами " .
                $pureTicketObject->getFullTicketNumberWithoutDash().".\n".
                "\nНомер вашей очереди " .
                $pureTicketObject->getFullTicketNumberWithoutDash().".";

            $this->sendSMS(
                $pureTicketObject->getCustomerPhoneNumber(),
                $message,
            );
        }

        event(
            new TicketPendingEvent(
                $pureTicketObject,
                $pureTicketObject->getVisitPurposeId(),
                "add",
            ),
        );

        return $pureTicketObject->jsonSerializeCreatedTicket();
    }

    private function sendSMS(string $customerPhoneNumber, string $message): void
    {
        SmsSenderJob::dispatch($customerPhoneNumber, $message);
    }
}
