<?php

namespace App\Modules\Ticket\Person\Requests;

use App\Modules\Status\Status;
use App\Requests\Person\ARequest;

class CRMStoreTicketRequest extends ARequest
{
    protected ?int $visitPurposeId;
    protected ?string $customerPhoneNumber;
    protected int $branchOfficeId;
    protected string $isSalomCache;
    protected string $newStatus;

    public function __construct(
        int $visitPurposeId,
        ?string $customerPhoneNumber,
        int $branchOfficeId
    ) {
        $this->visitPurposeId = $visitPurposeId;
        $this->customerPhoneNumber = str_replace(" ", "", $customerPhoneNumber);
        $this->branchOfficeId = $branchOfficeId;
        $this->newStatus = Status::$newStatus;
        $this->isSalomCache = true;
    }

    public function toArray(): array
    {
        return [
            "visitPurposeId" => $this->visitPurposeId,
            "customerPhoneNumber" => $this->customerPhoneNumber,
            "branchOfficeId" => $this->branchOfficeId,
        ];
    }

    public function validationRules(): array
    {
        return [
            "visitPurposeId" => "required",
            "customerPhoneNumber" => ["nullable", 'regex:/^\d{9}$/m'],
            "branchOfficeId" => "required",
        ];
    }

    public function getDataToStoreTicket(): array
    {
        return [
            "visitPurposeId" => $this->visitPurposeId,
            "customerPhoneNumber" => $this->customerPhoneNumber,
            "branchOfficeId" => $this->branchOfficeId,
            "currentStatusName" => $this->newStatus,
            "isSalomCache" => $this->isSalomCache,
        ];
    }
}
