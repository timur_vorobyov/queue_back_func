<?php

namespace App\Modules\Ticket\Person\Requests;

use App\Modules\Status\Status;
use App\Requests\Person\ARequest;

class StoreTicketRequest extends ARequest
{
    protected int $visitPurposeId;
    protected ?int $subVisitPurposeId;
    protected ?string $customerPhoneNumber;
    protected int $branchOfficeId;
    protected string $newStatus;

    public function __construct(
        int $visitPurposeId,
        ?string $customerPhoneNumber,
        int $branchOfficeId,
        ?int $subVisitPurposeId
    ) {
        $this->visitPurposeId = $visitPurposeId;
        $this->customerPhoneNumber = str_replace(" ", "", $customerPhoneNumber);
        $this->branchOfficeId = $branchOfficeId;
        $this->subVisitPurposeId = $subVisitPurposeId;
        $this->newStatus = Status::$newStatus;
    }

    public function toArray(): array
    {
        return [
            "visitPurposeId" => $this->visitPurposeId,
            "subVisitPurposeId" => $this->subVisitPurposeId,
            "customerPhoneNumber" => $this->customerPhoneNumber,
            "branchOfficeId" => $this->branchOfficeId,
        ];
    }

    public function validationRules(): array
    {
        return [
            "visitPurposeId" => "required",
            "subVisitPurposeId" => "required",
            "customerPhoneNumber" => [
                "nullable",
                'regex:/^\d{9}$/m',
            ],
            "branchOfficeId" => "required",
        ];
    }

    public function getDataToStoreTicket(): array
    {
        return [
            "visitPurposeId" => $this->visitPurposeId,
            "subVisitPurposeId" => $this->subVisitPurposeId,
            "customerPhoneNumber" => $this->customerPhoneNumber,
            "branchOfficeId" => $this->branchOfficeId,
            "currentStatusName" => $this->newStatus,
        ];
    }
}
