<?php

namespace App\Modules\Ticket\Person\Requests\UpdateTicketStrategy;

use App\Modules\Status\Status;

class AcceptTicketStrategyRequest extends AUpdateTicketStrategyRequest
{
    public function __construct(int $ticketId, int $userId)
    {
        $this->ticketId = $ticketId;
        $this->userId = $userId;
        $this->desiredStatusName = Status::$invitedStatus;
        $this->period = "invited_at";
    }

    public function toArray(): array
    {
        return [
            "ticketId" => $this->ticketId,
            "userId" => $this->userId,
        ];
    }

    public function validationRules(): array
    {
        return [
            "userId" => "required",
            "ticketId" => "required",
        ];
    }

    public function getDataToUpdateTicket(): array
    {
        return [
            "ticketId" => $this->ticketId,
            "userId" => $this->userId,
            "desiredStatusName" => $this->desiredStatusName,
            "period" => $this->period,
        ];
    }
}
