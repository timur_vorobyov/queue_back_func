<?php

namespace App\Modules\Ticket\Person\Requests\UpdateTicketStrategy;

use App\Requests\Person\ARequest;

abstract class AUpdateTicketStrategyRequest extends ARequest
{
    protected ?int $ticketId = null;
    protected ?int $userId = null;
    protected string $desiredStatusName;
    protected ?string $period;
    protected ?int $serviceId = null;
    protected ?int $visitPurposeId = null;
    protected ?int $subVisitPurposeId = null;

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function getTicketId(): ?int
    {
        return $this->ticketId;
    }
}
