<?php

namespace App\Modules\Ticket\Person\Events;

use App\Modules\Ticket\ETicket;
use App\Modules\Ticket\Person\Jobs\SmsSenderJob;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TicketAcceptEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private ETicket $ticket;
    private string $status;
    private string $type;
    private int|string $recipient;

    public function __construct(ETicket $ticket, int|string $recipient, string $status )
    {
        $this->ticket = $ticket;
        $this->recipient = $recipient;
        $this->status = $status;
        if ($recipient != 'all' && $this->status != 'delete') {
            $this->sendSMS();
        }
    }

    private function sendSMS(): void
    {
        $message = $this->prepareMessage();
        SmsSenderJob::dispatch(
            $this->ticket->getCustomerPhoneNumber(),
            $message,
        );
    }

    private function prepareMessage(): string
    {
        $deskNumber = mb_substr($this->ticket->getUser()->getDeskNumber(), 0, -1);
        $data = array($deskNumber, $this->ticket->getFullTicketNumberWithoutDash(), $this->ticket->getUser()->getFirstName());
        $pattern = array("desk_number", "ticket_number", "expert_name");
        $ruMessage = str_replace($pattern, $data, $this->ticket->getEVisitPurpose()->getRuMessage());
        $tjMessage = str_replace($pattern, $data, $this->ticket->getEVisitPurpose()->getTjMessage());
        return $tjMessage. "\n\n" .$ruMessage;
    }

    public function broadcastAs()
    {
        return "ticket-accepted";
    }

    public function broadcastWith()
    {
        return [
            'acceptedTicket' => $this->recipient == 'all' ? $this->ticket->jsonSerializeShortTicket() : $this->ticket->jsonSerialize(),
        ];
    }

    public function broadcastOn()
    {
        return new Channel("ticket.{$this->recipient}.{$this->ticket->getBranchOfficeId()}.{$this->ticket->getVisitPurposeId()}.accepted.{$this->status}");
    }
}
