<?php

namespace App\Modules\Ticket;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Service\Service;
use App\Modules\SubVisitPurpose\SubVisitPurpose;
use App\Modules\User\User;
use App\Modules\VisitPurpose\VisitPurpose;
use Database\Factories\TicketFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function newFactory()
    {
        return TicketFactory::new();
    }

    public function visitPurpose()
    {
        return $this->belongsTo(VisitPurpose::class);
    }

    public function subVisitPurpose()
    {
        return $this->belongsTo(SubVisitPurpose::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function branchOffice()
    {
        return $this->belongsTo(BranchOffice::class);
    }

    public function toDomainEntity(): ETicket
    {
        $relations = $this->getRelations();

        return new ETicket(
            $this->id,
            $this->crm_customer_id,
            $this->visit_purpose_id,
            $this->customer_phone_number,
            $this->branch_office_id,
            $this->ticket_number,
            $this->user_id,
            $this->status_id,
            $this->service_id,
            $this->invited_at,
            $this->served_at,
            $this->completed_at,
            $this->canceled_at,
            $this->created_at,
            key_exists('visitPurpose', $relations) && !is_null($this->visitPurpose) ? $this->visitPurpose->toDomainEntity() : null,
            key_exists('branchOffice', $relations) && !is_null($this->branchOffice) ? $this->branchOffice->toDomainEntity() : null,
            key_exists('user', $relations) && !is_null($this->user) ? $this->user->toDomainEntity() : null,
            key_exists('service', $relations) && !is_null($this->service) ? $this->service->toDomainEntity() : null,
            key_exists('subVisitPurpose', $relations) && !is_null($this->subVisitPurpose) ? $this->subVisitPurpose->toDomainEntity() : null,
            $this->is_salom_cache,
        );
    }
}
