<?php


namespace App\Http\Controllers\Admin;

use App\Modules\Ticket\Ticket;
use Illuminate\Http\Request;

class TicketController
{
    public function removeTickets(Request $request) {
        $ticketIds = $request->ticketIds;
        $isRemoved = Ticket::whereIn("id", $ticketIds)->delete();

        return response()->json([
            "message" => $isRemoved ? "INCOMING_TICKETS_ARE_REMOVED" : "INCOMING_TICKETS_ARE_NOT_REMOVED",
        ]);
    }
}
