<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\Role\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
        ]);

        if($validator->fails()) {
            return response()->json([
                "statusCode" => 422,
            ]);
        }

        $role = Role::create([
            "name" => $request->name,
            "guard_name" => "web",
        ]);

        $role->syncPermissions($request->permissionIds);

        return response()->json([
            "message" => $role->id ? "ROLE_IS_SAVED" : "ROLE_IS_NOT_SAVED",
        ]);
    }

    public function update(Request $request, Role $role)
    {
        $isUpdated = $role->update([
            "name" => $request->name,
        ]);

        $role->syncPermissions($request->permissionIds);

        return response()->json([
            "message" => $isUpdated ? "ROLE_IS_UPDATED" : "ROLE_IS_NOT_UPDATED",
        ]);
    }

    public function getPaginatedRolesList(Request $request)
    {
        $searchExpression = $request->searchExpression;

        $rolesQuery = Role::with('permissions');

        if ($searchExpression) {
            $rolesQuery->where("name", "LIKE", "%$searchExpression%");
        }

        $rolesList = $rolesQuery->paginate($request->limit);
        $pagination = [
            "totalRecords" => $rolesList->total(),
            "totalPages" => $rolesList->lastPage(),
            "currentPage" => $rolesList->currentPage(),
            "pageLimit" => $rolesList->perPage(),
        ];

        $rolesPureList = [];

        foreach ($rolesList->items() as $role) {
            array_push(
                $rolesPureList,
                $role->toDomainEntity()->superAdminJsonSerialize(),
            );
        }

        return response()->json([
            "pagination" => $pagination,
            "rolesPureList" => $rolesPureList,
        ]);

    }

    public function delete(Role $role)
    {
        $isRemoved = $role->delete();
        return response()->json([
            "message" => $isRemoved ? "ROLE_IS_REMOVED" : "ROLE_IS_NOT_REMOVED",
        ]);
    }
}
