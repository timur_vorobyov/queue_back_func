<?php


namespace App\Http\Controllers\Admin\SuperAdmin;


use App\Http\Controllers\Controller;
use App\Modules\Reports\ReportTicket\ReportTicket;
use App\Modules\Reports\ReportUserActionTime\ReportUserActionTime;
use App\Modules\Status\Status;
use App\Modules\Ticket\Ticket;
use App\Modules\User\User;
use Illuminate\Http\Request;


class ReportController extends Controller
{

    private Ticket $ticket;
    private Status $status;
    private User $user;
    private ReportUserActionTime $reportUserActionTime;

    public function __construct()
    {
        $this->ticket = app(Ticket::class);
        $this->status = app(Status::class);
        $this->user = app(User::class);
        $this->reportUserActionTime = app(ReportUserActionTime::class);
    }

    public function getPaginatedTicketsList(Request $request)
    {
        $branchOfficeId = $request->branchOfficeId;
        $dateFrom = $request->dateFrom == "1970-01-01" ? null : date('Y-m-d' , strtotime($request->dateFrom)).' 00:00:00';
        $dateTo = $request->dateTo == "1970-01-01" ? null : date('Y-m-d' , strtotime($request->dateTo)).' 23:59:59';
        $visitPurposeId = $request->visitPurposeId;
        $userId = $request->userId;
        $searchExpression = $request->searchExpression;
        $completedStatusId = $this->status::where('name', 'completed')->first()->id;
        $declinedStatusId = $this->status::where('name', 'declined')->first()->id;

        $ticketsQuery = $this->ticket::with("visitPurpose", "branchOffice", "user", "service")
            ->whereIn('status_id', [$completedStatusId, $declinedStatusId]);

        if ($branchOfficeId) {
            $ticketsQuery->where("branch_office_id", $branchOfficeId);
        }

        if($dateFrom && $dateTo) {
            $ticketsQuery
                ->where("created_at", '>=', $dateFrom)
                ->where("created_at", '<=' , $dateTo);
        }

        if ($visitPurposeId) {
            $ticketsQuery->where("visit_purpose_id", $visitPurposeId);
        }

        if ($userId) {
            $ticketsQuery->where("user_id", $userId);
        }
// Не работает поиск
        if ($searchExpression) {
            $ticketsQuery
                ->where("first_name", "LIKE", "%$searchExpression%")
                ->orWhere("last_name", "LIKE", "%$searchExpression%")
                ->orWhere("email", "LIKE", "%$searchExpression%");
        }

        $ticketsIds = $ticketsQuery->get()->pluck('id')->all();
        $modifiedTicketsId = '(' . implode(',', $ticketsIds) .')';

        if ($modifiedTicketsId != "()") {
            $ticketsList = ReportTicket::
            whereRaw("ticket_id IN $modifiedTicketsId")
                ->paginate($request->limit);

            $pagination = [
                "totalRecords" => $ticketsList->total(),
                "totalPages" => $ticketsList->lastPage(),
                "currentPage" => $ticketsList->currentPage(),
                "pageLimit" => $ticketsList->perPage(),
            ];

            $ticketsPureList = [];

            foreach ($ticketsList->items() as $ticket) {
                array_push(
                    $ticketsPureList,
                    $ticket->toDomainEntity()->reportJsonSerialize(),
                );
            }

            return response()->json([
                "pagination" => $pagination,
                "tickets" => $ticketsPureList,
            ]);
        }

        return response()->json([
            "pagination" => [],
            "tickets" => "",
        ]);

    }

    public function getPaginatedUserActionTimesList(Request $request)
    {
        $branchOfficeId = $request->branchOfficeId;
        $dateFrom = $request->dateFrom == "1970-01-01" ? null : date('Y-m-d' , strtotime($request->dateFrom)).' 00:00:00';
        $dateTo = $request->dateTo == "1970-01-01" ? null : date('Y-m-d' , strtotime($request->dateTo)).' 23:59:59';
        $statusId = $request->statusId;
        $userId = $request->userId;
        $searchExpression = $request->searchExpression;

        $userQuery = $this->user::with("branchOffice");

        if ($branchOfficeId) {
            $userQuery->where("branch_office_id", $branchOfficeId);
        }

        if ($statusId) {
            $userQuery->where("status_id", $statusId);
        }

        if ($searchExpression) {
            $userQuery
                ->where("first_name", "LIKE", "%$searchExpression%")
                ->orWhere("last_name", "LIKE", "%$searchExpression%")
                ->orWhere("email", "LIKE", "%$searchExpression%");
        }

        $userIds = $userQuery->get()->pluck('id')->all();

        $modifiedUsersId = '(' . implode(',', $userIds) .')';

        if ($modifiedUsersId != "()") {
            $userActionTimeQuery = $this->reportUserActionTime::with('user')
            ->whereRaw("user_id IN $modifiedUsersId");

            if($dateFrom && $dateTo) {
                $userActionTimeQuery
                    ->where("created_at", '>=', $dateFrom)
                    ->where("created_at", '<=' , $dateTo);
            }

            if ($userId) {
                $userActionTimeQuery->where("user_id", $userId);
            }

            $userActionTimesList = $userActionTimeQuery->paginate($request->limit);
            $userActionTimesPureList = [];

            foreach ($userActionTimesList->items() as $userActionTime) {
                array_push(
                    $userActionTimesPureList,
                    $userActionTime->toDomainEntity()->reportJsonSerialize(),
                );
            }

            $pagination = [
                "totalRecords" => $userActionTimesList->total(),
                "totalPages" => $userActionTimesList->lastPage(),
                "currentPage" => $userActionTimesList->currentPage(),
                "pageLimit" => $userActionTimesList->perPage(),
            ];

            return response()->json([
                "pagination" => $pagination,
                "userActionTimesList" => $userActionTimesPureList,
            ]);

        } else {
            return response()->json([
                "pagination" => [],
                "userActionTimesList" => [],
            ]);
        }

    }
}
