<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\Permission\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
        ]);

        if($validator->fails()) {
            return response()->json([
                "statusCode" => 422,
            ]);
        }

        $permission = Permission::create([
            "name" => $request->name,
            "guard_name" => "web",
        ]);

        return response()->json([
            "message" => $permission->id
                ? "PERMISSION_IS_SAVED"
                : "PERMISSION_IS_NOT_SAVED",
        ]);
    }

    public function update(Request $request, Permission $permission)
    {
        $isUpdated = $permission->update([
            "name" => $request->name,
        ]);

        return response()->json([
            "message" => $isUpdated
                ? "PERMISSION_IS_UPDATED"
                : "PERMISSION_IS_NOT_UPDATED",
        ]);
    }

    public function getPaginatedPermissionsList(Request $request)
    {
        $searchExpression = $request->searchExpression;

        $permissionQuery = Permission::query();

        if ($searchExpression) {
            $permissionQuery->where("name", "LIKE", "%$searchExpression%");
        }

        $permissionsList = $permissionQuery->paginate($request->limit);
        $pagination = [
            "totalRecords" => $permissionsList->total(),
            "totalPages" => $permissionsList->lastPage(),
            "currentPage" => $permissionsList->currentPage(),
            "pageLimit" => $permissionsList->perPage(),
        ];

        $permissionsPureList = [];

        foreach ($permissionsList->items() as $permission) {
            array_push(
                $permissionsPureList,
                $permission->toDomainEntity()->superAdminJsonSerialize(),
            );
        }

        return response()->json([
            "pagination" => $pagination,
            "permissionsPureList" => $permissionsPureList,
        ]);
    }

    public function delete(Permission $permission)
    {
        $isRemoved = $permission->delete();
        return response()->json([
            "message" => $isRemoved
                ? "PERMISSION_IS_REMOVED"
                : "PERMISSION_ID_NOT_REMOVED",
        ]);
    }
}
