<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Role\Role;
use App\Modules\User\Admin\SuperAdmin\UsersExport;
use App\Modules\User\User;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Excel;

class UserController extends Controller
{
    private User $user;
    private VisitPurpose $visitPurpose;
    private Role $role;
    private BranchOffice $branchOffice;
    private Validator $validator;
    private Excel $excel;


    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
        $this->user = app(User::class);
        $this->visitPurpose = app(VisitPurpose::class);
        $this->role = app(Role::class);
        $this->branchOffice = app(BranchOffice::class);
        $this->validator = app(Validator::class);
    }

    public function store(Request $request)
    {
        $validator = $this->validator::make($request->all(), [
            "branchOfficeId" => "required",
            "firstName" => "required",
            "lastName" => "required",
            "password" => "required",
            "email" => "required|unique:users",
            "roleId" => "required",
            "visitPurposesIds" => "required",
            "isActive" => "required",
            "isAllowedHaveSameDeskNumber" => "required",
        ]);

        if($validator->fails()) {

            $message = $validator->errors()->first();

            return response()->json([
                "statusCode" => 422,
                "message" => $message == "The email has already been taken." ? "EMAIL_ERROR" : $message
            ]);
        }

        $role = $this->role::find($request->roleId);

        $user = $this->user::create([
            "branch_office_id" => $request["branchOfficeId"],
            "first_name" => $request["firstName"],
            "last_name" => $request["lastName"],
            "email" => $request["email"],
            "password" => bcrypt($request["password"]),
            "is_active" => $request["isActive"],
            "is_allowed_have_same_desk_number" => $request["isAllowedHaveSameDeskNumber"],
        ]);

        $user->visitPurposes()->attach($request->visitPurposesIds);

        $user->assignRole($role->name);

        return response()->json([
            "message" => $user->id ? "USER_IS_SAVED" : "USER_IS_NOT_SAVED",
        ]);
    }

    public function update(Request $request, User $user)
    {
        $validator = $this->validator::make($request->all(), [
            "branchOfficeId" => "required",
            "firstName" => "required",
            "lastName" => "required",
            "email" => "required",
            "roleId" => "required",
            "visitPurposesIds" => "required",
            "isActive" => "required",
            "isAllowedHaveSameDeskNumber" => "required",
        ]);

        if($validator->fails()) {

            $message = $validator->errors()->first();

            return response()->json([
                "statusCode" => 422,
                "message" => $message == "The email has already been taken." ? "EMAIL_ERROR" : $message
            ]);
        }

        $role = $this->role::find($request->roleId);

        $isUpdated = $user->update([
            "branch_office_id" => $request["branchOfficeId"],
            "first_name" => $request["firstName"],
            "last_name" => $request["lastName"],
            "email" => $request["email"],
            "password" => $request["password"] ? bcrypt($request["password"]) : $user->password,
            "is_active" => $request["isActive"],
            "is_allowed_have_same_desk_number" => $request["isAllowedHaveSameDeskNumber"],
        ]);
        $user->visitPurposes()->sync($request->visitPurposesIds);
        $user->roles()->detach();
        $user->assignRole($role->name);

        return response()->json([
            "message" => $isUpdated ? "USER_IS_UPDATED" : "USER_IS_NOT_UPDATED",
        ]);
    }

    public function getPaginatedUsersList(Request $request)
    {
        $branchOfficeId = $request->branchOfficeId;
        $searchExpresion = $request->searchExpression;

        $usersQuery = $this->user::with("visitPurposes", "branchOffice", "userStatus", "roles");

        if ($branchOfficeId) {
            $usersQuery->where("branch_office_id", $branchOfficeId);
        }

        if ($searchExpresion) {
            $usersQuery
                ->where("first_name", "LIKE", "%$searchExpresion%")
                ->orWhere("last_name", "LIKE", "%$searchExpresion%")
                ->orWhere("email", "LIKE", "%$searchExpresion%");
        }

        $usersList = $usersQuery->paginate($request->limit);
        $pagination = [
            "totalRecords" => $usersList->total(),
            "totalPages" => $usersList->lastPage(),
            "currentPage" => $usersList->currentPage(),
            "pageLimit" => $usersList->perPage(),
        ];

        $usersPureList = [];

        foreach ($usersList->items() as $user) {
            array_push(
                $usersPureList,
                $user->toDomainEntity()->superAdminJsonSerialize(),
            );
        }

        return response()->json([
            "pagination" => $pagination,
            "usersPureList" => $usersPureList,
        ]);
    }

    public function delete(User $user)
    {
        $isRemoved = $user->delete();
        return response()->json([
            "message" => $isRemoved ? "USER_IS_REMOVED" : "USER_IS_NOT_REMOVED",
        ]);
    }

    public function getVisitPurposesList(string $branchOfficeId)
    {
        $visitPurposesList = $this->visitPurpose
            ::whereHas("branchOffices", function ($q) use ($branchOfficeId) {
                $q->where('branch_office_id', $branchOfficeId);
            })->get()
            ->map(function ($visitPurpose) {
                return $visitPurpose->toDomainEntity()->jsonSerialize();
            });

        return response()->json($visitPurposesList);
    }

    public function getBranchOfficeList(string $townId)
    {
        $branchOfficesList = $this->branchOffice
            ::where('town_id', $townId)
            ->get()
            ->map(function ($branchOffice) {
                return $branchOffice->toDomainEntity()->jsonSerialize();
            });

        return response()->json($branchOfficesList);
    }

    public function export(Request $request)
    {
        return $this->excel->download(new UsersExport($request->all()),'users.xlsx');
    }
}
