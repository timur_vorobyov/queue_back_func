<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\Town\Town;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TownController extends Controller
{

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
        ]);

        if($validator->fails()) {
            return response()->json([
                "statusCode" => 422,
            ]);
        }

        $town = Town::create([
            "name" => $request->name,
        ]);

        return response()->json([
            "message" => $town->id ? "TOWN_IS_SAVED" : "TOWN_IS_NOT_SAVED",
        ]);
    }

    public function update(Request $request, Town $town)
    {
        $isUpdated = $town->update([
            "name" => $request->name,
        ]);

        return response()->json([
            "message" => $isUpdated ? "TOWN_IS_UPDATED" : "TOWN_IS_NOT_UPDATED",
        ]);
    }

    public function getPaginatedTownsList(Request $request)
    {
        $searchExpression = $request->searchExpression;

        $townQuery = Town::query();

        if ($searchExpression) {
            $townQuery->where("name", "LIKE", "%$searchExpression%");
        }

        $townsList = $townQuery->paginate($request->limit);
        $pagination = [
            "totalRecords" => $townsList->total(),
            "totalPages" => $townsList->lastPage(),
            "currentPage" => $townsList->currentPage(),
            "pageLimit" => $townsList->perPage(),
        ];

        $townsPureList = [];

        foreach ($townsList->items() as $town) {
            array_push(
                $townsPureList,
                $town->toDomainEntity()->superAdminJsonSerialize(),
            );
        }

        return response()->json([
            "pagination" => $pagination,
            "townsPureList" => $townsPureList,
        ]);
    }

    public function delete(Town $town)
    {
        $isRemoved = $town->delete();
        return response()->json([
            "message" => $isRemoved ? "TOWN_IS_REMOVED" : "TOWN_IS_NOT_REMOVED",
        ]);
    }
}
