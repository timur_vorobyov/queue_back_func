<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\SubVisitPurpose\SubVisitPurpose;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubVisitPurposeController extends Controller
{
    private SubVisitPurpose $subVisitPurpose;
    private Validator $validator;

    public function __construct()
    {
        $this->subVisitPurpose = app(SubVisitPurpose::class);
        $this->validator = app(Validator::class);
    }

    public function store(Request $request)
    {
        $validator = $this->validator::make($request->all(), [
            "visitPurposeId" => "required",
            "name" => "required",
        ]);

        if($validator->fails()) {

            $message = $validator->errors()->first();

            return response()->json([
                "statusCode" => 422,
                "message" => $message
            ]);
        }

        $subVisitPurpose = $this->subVisitPurpose::create([
            "name" => $request->name,
            "visit_purpose_id" => $request->visitPurposeId,
        ]);


        return response()->json([
            "message" => $subVisitPurpose->id
                ? "SUB_VISIT_PURPOSE_IS_SAVED"
                : "SUB_VISIT_PURPOSE_IS_NOT_SAVED",
        ]);
    }

    public function update(Request $request, SubVisitPurpose $subVisitPurpose)
    {
        $subVisitPurposeIsUpdated = $subVisitPurpose->update([
            "name" => $request->name,
            "visit_purpose_id" => $request->visitPurposeId,
        ]);


        return response()->json([
            "message" => $subVisitPurposeIsUpdated
                ? "SUB_VISIT_PURPOSE_IS_UPDATED"
                : "SUB_VISIT_PURPOSE_IS_NOT_UPDATED",
        ]);
    }

    public function getPaginatedSubVisitPurposesList(Request $request)
    {
        $searchExpression = $request->searchExpression;
        $subVisitPurposesQuery = $this->subVisitPurpose::with('visitPurpose');

        if ($searchExpression) {
            $subVisitPurposesQuery->where("name", "LIKE", "%$searchExpression%");
        }

        $subVisitPurposesList = $subVisitPurposesQuery->paginate($request->limit);

        $pagination = [
            "totalRecords" => $subVisitPurposesList->total(),
            "totalPages" => $subVisitPurposesList->lastPage(),
            "currentPage" => $subVisitPurposesList->currentPage(),
            "pageLimit" => $subVisitPurposesList->perPage(),
        ];

        $subVisitPurposesPureList = [];

        foreach ($subVisitPurposesList->items() as $subVisitPurpose) {
            array_push(
                $subVisitPurposesPureList,
                $subVisitPurpose->toDomainEntity()->superAdminJsonSerialize(),
            );
        }

        return response()->json([
            "pagination" => $pagination,
            "subVisitPurposesPureList" => $subVisitPurposesPureList,
        ]);
    }

    public function delete(SubVisitPurpose $subVisitPurpose)
    {
        $isRemoved = $subVisitPurpose->delete();
        return response()->json([
            "message" => $isRemoved
                ? "SUB_VISIT_PURPOSE_IS_REMOVED"
                : "SUB_VISIT_PURPOSE_IS_NOT_REMOVED",
        ]);
    }
}
