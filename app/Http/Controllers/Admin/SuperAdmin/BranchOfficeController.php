<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\BranchOffice\BranchOffice;
use App\Modules\BranchOffice\Person\Requests\AssociateBranchOfficesRequest;
use App\Modules\Ticket\Person\Interfaces\ITicketRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BranchOfficeController extends Controller
{
    private ITicketRepository $ticketRepository;
    private BranchOffice $branchOffice;
    private Validator $validator;

    public function __construct()
    {
        $this->ticketRepository = app(ITicketRepository::class);
        $this->branchOffice = app(BranchOffice::class);
        $this->validator = app(Validator::class);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "initial" => "required|unique:branch_offices",
            "townId" => "required",
            "name" => "required",
            "timer" => "required",
            "visitPurposesIds" => "required",
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->first();

            return response()->json([
                "statusCode" => 422,
                "message" => $message == "The initial has already been taken." ? "INITIAL_ERROR" : $message,
            ]);
        }

        $branchOffice = BranchOffice::create([
            "initial" => $request->initial,
            "town_id" => $request->townId,
            "name" => $request->name,
            "timer" => $request->timer,
        ]);

        $branchOffice->visitPurposes()->attach($request->visitPurposesIds);

        $branchOfficeId = $branchOffice->id;

        if ($branchOfficeId && isset($request->crmBranchName)) {
            $params = $request->all();
            $params["id"] = $branchOfficeId;
            $this->ticketRepository->associateBranches($params);
        }

        return response()->json([
            "message" => $branchOfficeId
                ? "BRANCH_OFFICE_IS_SAVED"
                : "BRANCH_OFFICE_IS_NOT_SAVED",
        ]);
    }

    public function update(Request $request, BranchOffice $branchOffice)
    {
        $validator = Validator::make($request->all(), [
            "initial" => "required|unique:branch_offices",
            "townId" => "required",
            "name" => "required",
            "timer" => "required",
            "visitPurposesIds" => "required",
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->first();

            return response()->json([
                "statusCode" => 422,
                "message" => $message == "The initial has already been taken." ? "INITIAL_ERROR" : $message,
            ]);
        }

        $isUpdated = $branchOffice->update([
            "town_id" => $request->townId,
            "name" => $request->name,
            "initial" => $request->initial,
            "timer" => $request->timer,
        ]);

        $branchOffice->visitPurposes()->sync($request->visitPurposesIds);

        return response()->json([
            "message" => $isUpdated
                ? "BRANCH_OFFICE_IS_UPDATED"
                : "BRANCH_OFFICE_IS_NOT_UPDATED",
        ]);
    }

    public function getPaginatedBranchOfficesList(Request $request)
    {
        $townId = $request->townId;
        $searchExpression = $request->searchExpression;

        $branchOfficeQuery = $this->branchOffice::with('visitPurposes', 'town');

        if ($townId) {
            $branchOfficeQuery->where("town_id", $townId);
        }

        if ($searchExpression) {
            $branchOfficeQuery->where("name", "LIKE", "%$searchExpression%");
        }

        $branchOfficesList = $branchOfficeQuery->paginate($request->limit);
        $pagination = [
            "totalRecords" => $branchOfficesList->total(),
            "totalPages" => $branchOfficesList->lastPage(),
            "currentPage" => $branchOfficesList->currentPage(),
            "pageLimit" => $branchOfficesList->perPage(),
        ];

        $branchOfficesPureList = [];

        foreach ($branchOfficesList->items() as $branchOffice) {
            array_push(
                $branchOfficesPureList,
                $branchOffice->toDomainEntity()->superAdminJsonSerialize(),
            );
        }

        return response()->json([
            "pagination" => $pagination,
            "branchOfficesPureList" => $branchOfficesPureList,
        ]);
    }

    public function delete(BranchOffice $branchOffice)
    {
        $isRemoved = $branchOffice->delete();
        return response()->json([
            "message" => $isRemoved
                ? "BRANCH_OFFICE_IS_REMOVED"
                : "BRANCH_OFFICE_IS_NOT_REMOVED",
        ]);
    }

    /**
     * @param AssociateBranchOfficesRequest $request
     * @return JsonResponse
     */
    public function associateBranches(AssociateBranchOfficesRequest $request)
    {
        $areBranchesAssociated = $this->ticketRepository->associateBranches(
            $request->all(),
        );

        return response()->json([
            "message" => $areBranchesAssociated
                ? "BRANCHES_ARE_ASSOCIATED"
                : "BRANCHES_ARE_NOT_ASSOCIATED",
        ]);
    }
}
