<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VisitPurposeController extends Controller
{
    private VisitPurpose $visitPurpose;
    private Validator $validator;

    public function __construct()
    {
        $this->visitPurpose = app(VisitPurpose::class);
        $this->validator = app(Validator::class);
    }

    public function store(Request $request)
    {
        $validator = $this->validator::make($request->all(), [
            "visitPurposeTypeId" => "required",
            "servicesIds" => "required",
            "name" => "required",
            "series" => "required|unique:visit_purposes",
            "tjMessage" => "required",
            "isCashRegister" => "required",
        ]);

        if($validator->fails()) {

            $message = $validator->errors()->first();

            return response()->json([
                "statusCode" => 422,
                "message" => $message == "The series has already been saved." ? "INITIAL_ERROR" : $message
            ]);
        }

        $visitPurpose = $this->visitPurpose::create([
            "name" => $request->name,
            "visit_purpose_type_id" => $request->visitPurposeTypeId,
            "series" => $request->series,
            "tj_message" => $request->tjMessage,
            "ru_message" => $request->ruMessage,
            "is_prioratised" => $request->isPrioratised,
            "is_cash_register" => $request->isCashRegister,
        ]);

        $visitPurpose->services()->attach($request->servicesIds);

        return response()->json([
            "message" => $visitPurpose->id
                ? "VISIT_PURPOSE_IS_SAVED"
                : "VISIT_PURPOSE_IS_NOT_SAVED",
        ]);
    }

    public function update(Request $request, VisitPurpose $visitPurpose)
    {
        $visitPurposeIsUpdated = $visitPurpose->update([
            "name" => $request->name,
            "visit_purpose_type_id" => $request->visitPurposeTypeId,
            "series" => $request->series,
            "tj_message" => $request->tjMessage,
            "ru_message" => $request->ruMessage,
            "is_prioratised" => $request->isPrioratised,
            "is_cash_register" => $request->isCashRegister,
        ]);

        $visitPurpose->services()->sync($request->servicesIds);


        return response()->json([
            "message" => $visitPurposeIsUpdated
                ? "VISIT_PURPOSE_IS_UPDATED"
                : "VISIT_PURPOSE_IS_NOT_UPDATED",
        ]);
    }

    public function getPaginatedVisitPurposesList(Request $request)
    {
        $branchOfficeId = $request->branchOfficeId;
        $searchExpression = $request->searchExpression;

        $visitPurposesQuery = $this->visitPurpose::with('branchOffices', 'services', 'visitPurposeType');

        if ($branchOfficeId) {
            $visitPurposesQuery->whereHas("branchOffices", function ($q) use ($branchOfficeId) {
                $q->where('branch_office_id', $branchOfficeId);
            });
        }

        if ($searchExpression) {
            $visitPurposesQuery->where("name", "LIKE", "%$searchExpression%");
        }

        $visitPurposesList = $visitPurposesQuery->paginate($request->limit);
        $pagination = [
            "totalRecords" => $visitPurposesList->total(),
            "totalPages" => $visitPurposesList->lastPage(),
            "currentPage" => $visitPurposesList->currentPage(),
            "pageLimit" => $visitPurposesList->perPage(),
        ];

        $visitPurposesPureList = [];

        foreach ($visitPurposesList->items() as $visitPurpose) {
            array_push(
                $visitPurposesPureList,
                $visitPurpose->toDomainEntity()->superAdminJsonSerialize(),
            );
        }

        return response()->json([
            "pagination" => $pagination,
            "visitPurposesPureList" => $visitPurposesPureList,
        ]);
    }

    public function delete(VisitPurpose $visitPurpose)
    {
        $isRemoved = $visitPurpose->delete();
        return response()->json([
            "message" => $isRemoved
                ? "VISIT_PURPOSE_IS_REMOVED"
                : "VISIT_PURPOSE_IS_NOT_REMOVED",
        ]);
    }
}
