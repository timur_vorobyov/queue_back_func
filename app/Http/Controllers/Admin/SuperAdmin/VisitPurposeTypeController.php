<?php


namespace App\Http\Controllers\Admin\SuperAdmin;


use App\Http\Controllers\Controller;
use App\Modules\VisitPurposeType\VisitPurposeType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VisitPurposeTypeController extends Controller
{
    private VisitPurposeType $visitPurposeType;
    private Validator $validator;

    public function __construct()
    {
        $this->visitPurposeType = app(VisitPurposeType::class);
        $this->validator = app(Validator::class);
    }

    public function store(Request $request)
    {
        $validator = $this->validator::make($request->all(), [
            "name" => "required",
        ]);

        if($validator->fails()) {
            return response()->json([
                "statusCode" => 422,
            ]);
        }

        $visitPurposeType = $this->visitPurposeType::create([
            "name" => $request->name
        ]);

        return response()->json([
            "message" => $visitPurposeType->id
                ? "VISIT_PURPOSE_TYPE_IS_SAVED"
                : "VISIT_PURPOSE_TYPE_IS_NOT_SAVED",
        ]);
    }

    public function update(Request $request, VisitPurposeType $visitPurposeType)
    {
        $visitPurposeTypeIsUpdated = $visitPurposeType->update([
            "name" => $request->name,
        ]);

        return response()->json([
            "message" => $visitPurposeTypeIsUpdated
                ? "VISIT_PURPOSE_TYPE_IS_UPDATED"
                : "VISIT_PURPOSE_TYPE_IS_NOT_UPDATED",
        ]);
    }

    public function getPaginatedVisitPurposeTypesList(Request $request)
    {
        $searchExpression = $request->searchExpression;

        $visitPurposeTypesQuery = VisitPurposeType::query();


        if ($searchExpression) {
            $visitPurposeTypesQuery->where("name", "LIKE", "%$searchExpression%");
        }

        $visitPurposeTypesList = $visitPurposeTypesQuery->paginate($request->limit);
        $pagination = [
            "totalRecords" => $visitPurposeTypesList->total(),
            "totalPages" => $visitPurposeTypesList->lastPage(),
            "currentPage" => $visitPurposeTypesList->currentPage(),
            "pageLimit" => $visitPurposeTypesList->perPage(),
        ];

        $visitPurposeTypesPureList = [];

        foreach ($visitPurposeTypesList->items() as $visitPurposeType) {
            array_push(
                $visitPurposeTypesPureList,
                $visitPurposeType->toDomainEntity()->superAdminJsonSerialize(),
            );
        }

        return response()->json([
            "pagination" => $pagination,
            "visitPurposeTypesPureList" => $visitPurposeTypesPureList,
        ]);
    }

    public function delete(VisitPurposeType $visitPurposeType)
    {
        $isRemoved = $visitPurposeType->delete();
        return response()->json([
            "message" => $isRemoved
                ? "VISIT_PURPOSE_TYPE_IS_REMOVED"
                : "VISIT_PURPOSE_TYPE_IS_NOT_REMOVED",
        ]);
    }
}
