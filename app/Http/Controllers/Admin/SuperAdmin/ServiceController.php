<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\Service\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "isActive" => "required",
        ]);

        if($validator->fails()) {
            return response()->json([
                "statusCode" => 422,
            ]);
        }

        $service = Service::create([
            "name" => $request->name,
            "is_active" => $request->isActive,
        ]);

        return response()->json([
            "message" => $service->id
                ? "SERVICE_IS_SAVED"
                : "SERVICE_IS_NOT_SAVED",
        ]);
    }

    public function update(Request $request, Service $service)
    {
        $isUpdated = $service->update([
            "name" => $request->name,
            "is_active" => $request->isActive,
        ]);

        return response()->json([
            "message" => $isUpdated
                ? "SERVICE_IS_UPDATED"
                : "SERVICE_IS_NOT_UPDATED",
        ]);
    }

    public function getPaginatedServicesList(Request $request)
    {
        $visitPurposeId = $request->visitPurposeId;
        $searchExpression = $request->searchExpression;

        $servicesQuery = Service::query();

        if ($visitPurposeId) {
            $servicesQuery->whereHas("visitPurposes", function ($q) use ($visitPurposeId) {
                $q->where('visit_purpose_id', $visitPurposeId);
            });
        }

        if ($searchExpression) {
            $servicesQuery->where("name", "LIKE", "%$searchExpression%");
        }

        $servicesList = $servicesQuery->paginate($request->limit);
        $pagination = [
            "totalRecords" => $servicesList->total(),
            "totalPages" => $servicesList->lastPage(),
            "currentPage" => $servicesList->currentPage(),
            "pageLimit" => $servicesList->perPage(),
        ];

        $servicesPureList = [];

        foreach ($servicesList->items() as $service) {
            array_push(
                $servicesPureList,
                $service->toDomainEntity()->superAdminJsonSerialize(),
            );
        }

        return response()->json([
            "pagination" => $pagination,
            "servicesPureList" => $servicesPureList,
        ]);
    }

    public function delete(Service $service)
    {
        $isRemoved = $service->delete();
        return response()->json([
            "message" => $isRemoved
                ? "SERVICE_IS_REMOVED"
                : "SERVICE_IS_NOT_REMOVED",
        ]);
    }
}
