<?php


namespace App\Http\Controllers\Admin;


use App\Modules\Status\Status;
use App\Modules\Ticket\Person\Events\TicketPendingEvent;
use App\Modules\Ticket\Ticket;
use Carbon\Carbon;

class BasketController
{
    private Ticket $ticket;
    private Status $status;

    public function __construct()
    {
        $this->ticket = app(Ticket::class);
        $this->status = app(Status::class);
    }

    public function getDeclinedTicketsList(int $branchOfficeId): object {
        $dateFrom = Carbon::today()->toDateString().' 00:00:00';
        $dateTo = Carbon::today()->toDateString() .' 23:59:59';
        $status = $this->status
            ::where("name", 'declined')
            ->first();

        $ticketsQuery = $this->ticket::with('visitPurpose','branchOffice')
            ->where('status_id',$status->id)
            ->where("created_at", '>=', $dateFrom)
            ->where("created_at", '<=' , $dateTo);

        if($branchOfficeId) {
            $ticketsQuery->where('branch_office_id', $branchOfficeId);
        }

        $tickets = $ticketsQuery->get()
            ->map(function ($ticket) {
                return $ticket->toDomainEntity()->adminJsonSerialize();
            });

        return response()->json($tickets);
    }

    public function recoverTicket (Ticket $ticket) {
        $status = $this->status::where('name', 'new')->first();
        $isUpdated = $ticket->update(['status_id' => $status->id, 'user_id' => null, 'invited_at' => null, 'canceled_at' => null]);
        $updatedTicket = $this->ticket::with('visitPurpose', 'branchOffice', 'service')
            ->where('id', $ticket->id)
            ->first();
        event(
            new TicketPendingEvent(
                $updatedTicket->toDomainEntity(),
                $updatedTicket->toDomainEntity()->getVisitPurposeId(),
                "recover",
            ),
        );

        return response()->json([
            "message" => $isUpdated ? "TicketController is recovered" : "TicketController is not recovered",
        ]);
    }
}
