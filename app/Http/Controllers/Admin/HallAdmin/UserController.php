<?php

namespace App\Http\Controllers\Admin\HallAdmin;

use App\Http\Controllers\Controller;
use App\Modules\User\Person\UserRepository;
use App\Modules\User\User;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private UserRepository $userRepository;
    private VisitPurpose $visitPurpose;

    public function __construct()
    {
        $this->userRepository = app(UserRepository::class);
        $this->visitPurpose = app(VisitPurpose::class);
    }

    public function update(Request $request, User $user)
    {
        $branchInitial = $user->branchOffice->initial;
        $fullDeskNumber = $request->deskNumber . $branchInitial;
        $countSavedDeskNumber = $this->userRepository->getDeskNumberExistingCount($fullDeskNumber);

        if($countSavedDeskNumber == 1) {
            return response()->json([
                "message" => "Desk number already exists",
            ]);
        }

        $isUpdated = $user->update([
            "desk_number" => $request["deskNumber"] . $branchInitial,
        ]);

        return response()->json([
            "message" => $isUpdated ? "User is updated" : "User is not updated",
        ]);
    }

    public function getUsersList(Request $request)
    {
        $branchOfficeId = $request->branchOfficeId;
        $searchExpresion = $request->searchExpression;

        $usersQuery = User::with("visitPurposes")
            ->where("branch_office_id", $branchOfficeId);

        if ($searchExpresion) {
            $usersQuery
                ->where("first_name", "LIKE", "%$searchExpresion%")
                ->orWhere("last_name", "LIKE", "%$searchExpresion%")
                ->orWhere("email", "LIKE", "%$searchExpresion%");
        }

        $usersList = $usersQuery->paginate($request->limit);
        $pagination = [
            "totalRecords" => $usersList->total(),
            "totalPages" => $usersList->lastPage(),
            "currentPage" => $usersList->currentPage(),
            "pageLimit" => $usersList->perPage(),
        ];

        $usersPureList = [];

        foreach ($usersList->items() as $user) {
            array_push(
                $usersPureList,
                $user->toDomainEntity()->hallAdminJsonSerialize(),
            );
        }

        return response()->json([
            "pagination" => $pagination,
            "usersPureList" => $usersPureList,
        ]);
    }

    public function delete(User $user)
    {
        $isRemoved = $user->delete();
        return response()->json([
            "message" => $isRemoved ? "User is removed" : "User is not removed",
        ]);
    }
}
