<?php


namespace App\Http\Controllers;


use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Permission\Permission;
use App\Modules\Role\Role;
use App\Modules\Service\Service;
use App\Modules\Status\Status;
use App\Modules\Ticket\Ticket;
use App\Modules\Town\Town;
use App\Modules\User\User;
use App\Modules\UserStatus\UserStatus;
use App\Modules\VisitPurpose\VisitPurpose;
use App\Modules\VisitPurposeType\VisitPurposeType;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ModulesDataListMediator extends Controller
{
    public function getUsersList()
    {
        return User::with('visitPurposes', 'branchOffice', 'roles', "userStatus")->get()->map(function ($user) {
            return $user->toDomainEntity()->jsonSerialize();
        });
    }

    public function getServicesList()
    {
        return Service::all()->map(function ($service) {
            return $service->toDomainEntity()->jsonSerialize();
        });
    }


    public function getBranchOfficesList(Request $request)
    {
        $townId = $request->townId;
        $branchOfficeQuery = BranchOffice::with('visitPurposes', 'town');
        if($townId) {
            $branchOfficeQuery->where('town_id', $townId);
        }
        return $branchOfficeQuery->get()->map(function ($branchOffice) {
            return $branchOffice->toDomainEntity()->jsonSerialize();
        });
    }

    public function getVisitPurposesList(Request $request)
    {
        $branchOfficeId = $request->branchOfficeId;
        $visitPurposesQuery = VisitPurpose::with('branchOffices', 'services', 'visitPurposeType');

        if ($branchOfficeId) {
            $visitPurposesQuery->whereHas("branchOffices", function ($q) use ($branchOfficeId) {
                $q->where('branch_office_id', $branchOfficeId);
            });
        }

        return $visitPurposesQuery->get()->map(function ($visitPurpose) {
            return $visitPurpose->toDomainEntity()->jsonSerialize();
        });
    }

    public function getVisitPurposeTypesList()
    {
        return VisitPurposeType::all()->map(function ($visitPurposeType) {
            return $visitPurposeType->toDomainEntity()->jsonSerialize();
        });
    }

    public function getUserStatusesList()
    {
        return UserStatus::all()->toArray();
    }

    public function getTownsList()
    {
        return Town::all()->map(function ($town) {
            return $town->toDomainEntity()->jsonSerialize();
        });
    }

    public function getRolesList()
    {
        return Role::all()->map(function ($role) {
            return $role->toDomainEntity()->jsonSerialize();
        });
    }

    public function getPermissionsList()
    {
        return Permission::all()->map(function ($permission) {
            return $permission->toDomainEntity()->jsonSerialize();
        });
    }

    /**
     * @return JsonResponse
     */
    public function getVisitPurposes(): JsonResponse
    {
        return response()->json(VisitPurpose::all());
    }

    public function getIncomingTicketsList(Request $request)
    {
        $currentStatus = Status::where("name", "new")->first();
        $ticketQuery = Ticket::with("visitPurpose", "branchOffice", "subVisitPurpose")
            ->whereNull('user_id');

        if((int)$request->branchOfficeId){
            $ticketQuery->where("branch_office_id", $request->branchOfficeId);
        }

        if((int)$request->visitPurposeId){
            $ticketQuery->where("visit_purpose_id", $request->visitPurposeId);
        }

        $tickets = $ticketQuery
            ->where("status_id", $currentStatus->id)
            ->whereDate('created_at', Carbon::today())
            ->get();

        return $tickets->map(function ($ticket) {
            return $ticket->toDomainEntity()->jsonSerialize();
        });
    }
}
