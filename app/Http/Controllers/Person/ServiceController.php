<?php

namespace App\Http\Controllers\Person;

use App\Modules\Service\Person\Interfaces\IServiceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServiceController
{

    public function showServices(Request $request)
    {
        Validator::make($request->all(), [
            "visitPurposeId" => "required",
        ]);

        $service = app(IServiceRepository::class)
            ->getServiceListFilteredByVisitPurposeId($request->visitPurposeId);

        return response()->json($service);
    }
}
