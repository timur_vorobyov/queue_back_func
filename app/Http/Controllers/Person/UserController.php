<?php

namespace App\Http\Controllers\Person;

use App\Http\Controllers\Controller;
use App\Modules\User\Person\Events\UserStatisticsDataEvent;
use App\Modules\User\Person\Requests\LoginUserRequest;
use App\Modules\User\Person\Requests\UpdateUserSettingsRequest;
use App\Modules\User\Person\Requests\UpdateUserStatusRequest;
use App\Modules\User\Person\Service\StatisticsRecipientService;
use App\Modules\User\Person\UseCases\LoginUserUseCase;
use App\Modules\User\Person\UseCases\UpdateUserSettingsUseCase;
use App\Modules\User\Person\UseCases\UpdateUserStatusUseCase;
use App\Modules\User\Person\UserRepository;
use App\Modules\User\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function login(Request $request)
    {
        $useCaseRequest = new LoginUserRequest(
            $request->email,
            $request->password,
        );

        $useCase = app(LoginUserUseCase::class);
        $data = $useCase->perform($useCaseRequest);

        return response()->json([
            "message" => $data["token"]
                ? "You are logged in"
                : "We can`t find an account with this credentials",
            "accessToken" => $data["token"],
            "id" => $data["userId"],
        ]);
    }

    public function updateWorkData(Request $request)
    {
        $useCaseRequest = new UpdateUserSettingsRequest(
            $request->userId,
            $request->branchOfficeId,
            $request->visitPurposesIds,
            $request->deskNumber,
            $request->userIdWithSameDeskNumber,
        );

        $useCase = app(UpdateUserSettingsUseCase::class);
        $useCase->perform($useCaseRequest);

        return response()->json([
            "message" => "USER'S_SETTINGS_ARE_UPDATED",
        ]);
    }

    public function isDeskNumberFree(Request $request)
    {
        $userRepository = app(UserRepository::class);
        $user = User::with('branchOffice')->find($request->userId)->toDomainEntity();
        $branchInitial = $user->getEBranchOffice()->getInitial();
        $fullDeskNumber = $request->deskNumber . $branchInitial;
        $countSavedDeskNumber = $userRepository->getDeskNumberExistingCount($fullDeskNumber);

        if($countSavedDeskNumber >= 1) {
            $userWithSameDeskNumber = $userRepository->getUserFilteredByDeskNumber($fullDeskNumber);

            if($user->getId() == $userWithSameDeskNumber->getId()) {
                return response()->json([
                    "message" => "UPDATE_IS_ALLOWED"
                ]);
            }

            if($userRepository->isUserHasClient($userWithSameDeskNumber->getId())) {
                return response()->json([
                    "message" => "USER_WITH_THE_SAME_DESK_NUMBER_HAS_CLIENT"
                ]);
            }

            if(!$userWithSameDeskNumber->isAllowedHaveSameDeskNumber() ||
                ($user->isAllowedHaveSameDeskNumber() && $userWithSameDeskNumber->isAllowedHaveSameDeskNumber())
            ) {
                return response()->json([
                    "message" => "UPDATE_IS_NOT_ALLOWED",
                    'userFirstName' => $userWithSameDeskNumber->getFirstName(),
                    'userIdWithSameDeskNumber' => $userWithSameDeskNumber->getId()
                ]);
            }

        }

        return response()->json([
            "message" => "UPDATE_IS_ALLOWED"
        ]);
    }

    public function getUsersAverageServiceTime(Request $request)
    {
        $dataRecipientService = app(StatisticsRecipientService::class);
        $usersAverageServiceTime = $dataRecipientService->getUsersAverageServiceTime($request->branchOfficeId);

        return response()->json([
            "usersAverageServiceTime" => $usersAverageServiceTime,
        ]);
    }

    public function getUserAverageServiceTime(Request $request)
    {
        $dataRecipientService = app(StatisticsRecipientService::class);
        $userAverageServiceTime = $dataRecipientService->getUserAverageServiceTime(
            $request->userId,
            $request->visitPurposesIds,
        );

        return response()->json([
            "userAverageServiceTime" => $userAverageServiceTime,
        ]);
    }

    public function getUsersMaxServiceTime(Request $request)
    {
        $dataRecipientService = app(StatisticsRecipientService::class);
        $usersMaxServiceTime = $dataRecipientService->getUsersMaxServiceTime($request->branchOfficeId);
        return response()->json([
            "usersMaxServiceTime" => $usersMaxServiceTime,
        ]);
    }

    public function getCustomersAmountUserServed(
        Request $request
    ) {
        $dataRecipientService = app(StatisticsRecipientService::class);
        $customersAmountUserServed = $dataRecipientService->getCustomersAmountUserServed(
            $request->userId,
        );
        return response()->json([
            "customersAmountUserServed" => $customersAmountUserServed,
        ]);
    }

    public function getUserData(int $id)
    {
        $user = User::with('visitPurposes', 'branchOffice', 'userStatus', 'roles')->find($id);

        if (!$user) {
            return "User not found";
        }

        return response()->json($user->toDomainEntity()->jsonSerialize());
    }

    public function updateStatisticsData(Request $request)
    {
        $dataRecipientService = app(StatisticsRecipientService::class);
        $usersAverageServiceTime = $dataRecipientService->getUsersAverageServiceTime($request->branchOfficeId);
        $userAverageServiceTime = $dataRecipientService->getUserAverageServiceTime(
            $request->userId,
            $request->visitPurposesIds,
        );
        $customersAmountUserServed = $dataRecipientService->getCustomersAmountUserServed(
            $request->userId
        );
        $usersMaxServiceTime = $dataRecipientService->getUsersMaxServiceTime($request->branchOfficeId);
        event(new UserStatisticsDataEvent(
            $request->userId,
            $usersAverageServiceTime,
            $userAverageServiceTime,
            $customersAmountUserServed,
            $usersMaxServiceTime,
        ));
    }

    public function updateUserStatus(Request $request)
    {
        $userRepository = app(UserRepository::class);
        $useCaseRequest = new UpdateUserStatusRequest(
            $request->userId,
            $request->statusId
        );

        $useCase = app(UpdateUserStatusUseCase::class);
        $isUpdated = $useCase->perform($useCaseRequest);
        $userData = $userRepository->getUserDataFilteredById($request->userId);

        return response()->json([
            "message" => $isUpdated ? "STATUS_IS_UPDATED" : "STATUS_IS_NOT_UPDATED",
            "userData" => $userData
        ]);
    }
}
