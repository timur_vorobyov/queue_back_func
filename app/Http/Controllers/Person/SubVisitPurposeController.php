<?php


namespace App\Http\Controllers\Person;


use App\Modules\SubVisitPurpose\Person\Interfaces\ISubVisitPurposeRepository;
use App\Modules\SubVisitPurpose\Person\SubVisitPurposeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubVisitPurposeController
{
    public function showSubVisitPurposes(Request $request)
    {
        Validator::make($request->all(), [
            "visitPurposeId" => "required",
        ]);

        $subVisitPurposes = app(SubVisitPurposeRepository::class)
            ->getSubVisitPurposesListFilteredByVisitPurposeId($request->visitPurposeId);

        return response()->json($subVisitPurposes);
    }
}
