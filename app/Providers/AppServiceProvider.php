<?php

namespace App\Providers;

use App\Modules\BranchOffice\Person\Interfaces\IBranchOfficeRepository;
use App\Modules\BranchOffice\Person\BranchOfficeRepository;
use App\Modules\Service\Person\Interfaces\IServiceRepository;
use App\Modules\Service\Person\ServiceRepository;
use App\Modules\Ticket\Person\Interfaces\ITicketRepository;
use App\Modules\Ticket\Person\TicketRepository;
use App\Modules\Town\Person\Interfaces\ITownRepository;
use App\Modules\Town\Person\TownRepository;
use App\Modules\User\Person\Interfaces\IUserRepository;
use App\Modules\User\Person\UserRepository;
use App\Modules\VisitPurpose\Person\Interfaces\IVisitPurposeRepository;
use App\Modules\VisitPurpose\Person\VisitPurposeRepository;
use App\Services\CallSenderService;
use App\Services\CrmService;
use App\Services\Interfaces\ICallSenderService;
use App\Services\Interfaces\ICustomerSearchService;
use App\Services\Interfaces\ISmsService;
use App\Services\Interfaces\ITelegramMessageSenderService;
use App\Services\SmsService;
use App\Services\TelegramMessageSenderService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ITicketRepository::class, TicketRepository::class);
        $this->app->bind(IUserRepository::class, UserRepository::class);
        $this->app->bind(IBranchOfficeRepository::class, BranchOfficeRepository::class);
        $this->app->bind(ITownRepository::class, TownRepository::class);
        $this->app->bind(IVisitPurposeRepository::class, VisitPurposeRepository::class);
        $this->app->bind(IServiceRepository::class, ServiceRepository::class);
        $this->app->bind(ITelegramMessageSenderService::class, TelegramMessageSenderService::class);
        $this->app->bind(ISmsService::class, SmsService::class);
        $this->app->bind(ICallSenderService::class, CallSenderService::class);
        $this->app->bind(ICustomerSearchService::class, CrmService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
