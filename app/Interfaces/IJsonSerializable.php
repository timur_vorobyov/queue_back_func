<?php

namespace App\Interfaces;

interface IJsonSerializable
{
    //Data transfer objects
    public function superAdminJsonSerialize(): array;
    public function adminJsonSerialize(): array;
    public function jsonSerialize(): array;
}
