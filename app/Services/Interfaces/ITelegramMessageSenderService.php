<?php

namespace App\Services\Interfaces;

interface ITelegramMessageSenderService
{
    public function sendMessage(string $message): void;
}
