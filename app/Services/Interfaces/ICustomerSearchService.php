<?php


namespace App\Services\Interfaces;


use Illuminate\Http\JsonResponse;
use stdClass;

interface ICustomerSearchService
{
    public function searchFromCRM(string $customerPhoneNumber): stdClass | bool;
    public function getCRMBranches(): JsonResponse;
}
