<?php


namespace App\Services;


use App\Services\Interfaces\ITelegramMessageSenderService;
use Basis\Nats\Client;
use Basis\Nats\Configuration;
use Basis\Nats\Message\Payload;
use DateTime;
use DateTimeZone;

class SmsSurveyService
{
    private ITelegramMessageSenderService $telegramMessageSender;

    public function __construct(
        ITelegramMessageSenderService $telegramMessageSender
    ) {
        $this->telegramMessageSender = $telegramMessageSender;
    }

    public function send(
        string $phoneNumber,
        string $visitPurposeName,
        int $userId,
        string $userFullName,
        string $townName,
        string $branchOfficeName,
        string $completedAt
): void
    {
        try {
            $configuration = new Configuration([
                'host' => '192.168.15.173',
                'jwt' => null,
                'lang' => 'php',
                'pass' => null,
                'pedantic' => false,
                'port' => 30222,
                'reconnect' => true,
                'timeout' => 1,
                'token' => '2NuQt76AqTGv4C5ax9PzftyDVRt3Gwteq8vwz',
                'user' => null,
                'verbose' => false,
                'version' => 'dev'
            ]);

            $configuration->setDelay(0.001);
            $client = new Client($configuration);
            $client->ping();

            $given = new DateTime($completedAt);
            $given->setTimezone(new DateTimeZone("UTC"));


            $body = json_encode([
                "service_id" => "alifqueue",
                "customer_id"=> $phoneNumber,
                "customer_id_type" => "phone",
                "customer_name" => "",
                "subject"=> [$visitPurposeName],
                "employee_id"=> "$userId",
                "employee_name"=> $userFullName,
                "employee_region"=> $townName,
                "employee_point"=> $branchOfficeName,
                "lang"=> "tj",
                "servedate" => $given->format('Y-m-d\TH:i:s\Z'),
            ], JSON_PRETTY_PRINT);
            $header = ['Api-Key' => 'ibknvya6QRGCeK7iT1jcWBZEnIGpz6xDcHRKbJbh7aF1QNhiBkEIwK8scE39iG21'];

            $client->publish('csat',new Payload($body,$header));

        } catch (\Exception $e) {
            $this->telegramMessageSender->sendMessage($e->getMessage());
        }
    }

}
