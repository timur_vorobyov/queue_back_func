<?php

namespace App\Services;

use App\Services\Interfaces\ISmsService;
use App\Services\Interfaces\ITelegramMessageSenderService;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;

class SmsService implements ISmsService
{
    private string $smsCenterToken;
    private ITelegramMessageSenderService $telegramMessageSender;

    private string $from = "alif.navbat";

    public function __construct(
        ITelegramMessageSenderService $telegramMessageSender
    ) {
        $this->smsCenterToken = "7ae8b01c-f793-4fde-be15-6764d4d70cda";
        $this->telegramMessageSender = $telegramMessageSender;
    }

    public function send(string $customerPhoneNumber, string $message): void
    {
        $smsCenterUrl = "https://sms2.aliftech.net/api/v1/sms";
        $client = new Client(['base_uri' => $smsCenterUrl, 'verify' => false]);
        try {
            $response = $client->request('POST', '', [
                "headers" => [
                    'X-Api-Key' => $this->smsCenterToken,
                    'Content-type' => 'application/json',
                    'charset' => 'utf-8'
                ],
                "body" => json_encode([
                    "phoneNumber" => "992".$customerPhoneNumber,
                    "senderAddress" => $this->from,
                    "text" => $message,
                    "priority" => 2,
                    "expiresIn"=> 0
                ]),
            ]);

            if ($response->getStatusCode() != 200) {
                $this->sendExceptionToTelegram($response);
            }

            if ($response->getStatusCode() == 200) {
                Log::channel("sms")->info(
                    "Send to " . $customerPhoneNumber . ", Massage: " . $message,
                );
            }
        } catch (\Exception $e) {
            $this->telegramMessageSender->sendMessage($e->getMessage());
        }
    }

    private function sendExceptionToTelegram(ResponseInterface $response): void
    {
        $message =
            config("app.name") .
            ": SMS not sent: { code:" .
            $response->getStatusCode() .
            ", message:" .
            $response->getReasonPhrase() .
            "}";
        $this->telegramMessageSender->sendMessage($message);
    }

}
