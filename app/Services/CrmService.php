<?php


namespace App\Services;


use App\Services\Interfaces\ICustomerSearchService;
use App\Services\Interfaces\ITelegramMessageSenderService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use stdClass;

class CrmService implements ICustomerSearchService
{
    private ITelegramMessageSenderService $telegramMessageSender;

    public function __construct(ITelegramMessageSenderService $telegramMessageSender)
    {
        $this->telegramMessageSender = $telegramMessageSender;
    }

    public function searchFromCRM(string $customerPhoneNumber): stdClass | bool
    {

        $crmUrl = env('CRM_SEARCH_API_URL');
        $crmToken = env('CRM_SEARCH_API_TOKEN');
        $phone = "992$customerPhoneNumber";
        try {
            $client = new Client();
            $response = $client->request('POST', $crmUrl, [
                'http_errors' => false,
                'verify' => false,
                'form_params' => ['api_token' => $crmToken, 'phone' => $phone],
                'timeout' => 4, // Response timeout
                'connect_timeout' => 4, // Connection timeout
            ]);

            if ($response->getStatusCode() != 200) {
                return false;
            }

            $content = json_decode($response->getBody()->getContents());

            if ($content->hasClient) {
                Log::channel("crm-search")->info(
                    "The customer's phone number ". $customerPhoneNumber ." was found"
                );
            }

            return $content;
        } catch (\Exception $e) {
            $this->telegramMessageSender->sendMessage($e->getMessage());
            return false;
        }
    }

    /**
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function getCRMBranches(): JsonResponse
    {
        try {
            $httpClient = new Client([
                "base_uri" =>
                    config("app.CRM_URL") . "/api_service/branch/v1/all",
            ]);
            $response = $httpClient->request("POST", "", [
                "headers" => [
                    "Accept" => "application/json",
                    "Content-type" => "application/json",
                ],
                "body" => json_encode([
                    "api_token" => config("app.CRM_API_TOKEN"),
                ]),
            ]);

            $result = $response->getBody()->getContents();

            return response()->json(json_decode($result));
        } catch (\Exception $e) {
            \Log::info(["getCRMBranches Exception" => $e]);

            return response()->json($e->getMessage());
        }
    }


}
