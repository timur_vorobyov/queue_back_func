# Queue

### Infrastructure
![](q_infrastructure.png)


## Getting started
### Requirements

The Laravel framework has a few system requirements. You should ensure that your web server has the following minimum PHP version and extensions:

- PHP >= 8.0
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- SQLITE 3

### Installation

Clone the repository

    git clone git@github.com:alifcapital/queue.backend.git
    
Make sure that your current branch is `master` if you are in production server and `develop` if you are in test server. Separation the branches is carried out according to [git flow approach](https://www.atlassian.com/ru/git/tutorials/comparing-workflows/gitflow-workflow).
    
Install all the dependencies using composer
    
    composer install --ignore-platform-reqs

Copy the example env file and make the required configuration changes in the .env file
  
    cp .env.example .env
    
Generate a new application key

    php artisan key:generate
    
Generate a new JWT authentication secret key

    php artisan jwt:secret
    
Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Run the database seeder and you're done. It will be created statuses and roles: `super admin`, `hall admin` and `user`, also the user data will be registered: email `user@gmail.com` and password `123456789` being assigned to `super admin` role

    php artisan db:seed

**Note** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

    php artisan migrate:refresh
    
Run queue worker

    php artisan queue:work
    
Run laravel-echo-server

    laravel-echo-server start
    

#### Laravel-Echo installation(Ubuntu)

- `npm install -g laravel-echo-server`
- `laravel-echo-server init`
    
##### Development laravel-echo-server.json

    {
     	"authHost": "http://localhost",
     	"authEndpoint": "/broadcasting/auth",
     	"clients": [
     		{
     			"appId": "00b9ac5ba8bace5b",
     			"key": "46046ddb90756b148b059dc6f4f9f440"
     		}
     	],
     	"database": "redis",
     	"databaseConfig": {
     		"redis": {},
     		"sqlite": {
     			"databasePath": "/database/laravel-echo-server.sqlite"
     		}
     	},
     	"devMode": true,
     	"port": "6001",
     	"protocol": "http",
     	"socketio": {},
     	"secureOptions": 67108864,
     	"sslCertPath": "",
     	"sslKeyPath": "",
     	"sslCertChainPath": "",
     	"sslPassphrase": "",
     	"subscribers": {
     		"http": true,
     		"redis": true
     	},
     	"apiOriginAllow": {}
     }
##### Production laravel-echo-server.json 
       {
       	"authHost": "host",
       	"authEndpoint": "/broadcasting/auth",
       	"clients": [
       		{
       			"appId": "00b9ac5ba8bace5b",
       			"key": "46046ddb90756b148b059dc6f4f9f440"
       		}
       	],
       	"database": "redis",
       	"databaseConfig": {
       		"redis": {},
       		"sqlite": {
       			"databasePath": "/database/laravel-echo-server.sqlite"
       		}
       	},
       	"devMode": true,
       	"port": "6001",
       	"protocol": "https",
       	"socketio": {},
       	"secureOptions": 67108864,
               "sslCertPath": "path to cert",
       	"sslKeyPath": "path to key",
       	"sslCertChainPath": "",
       	"sslPassphrase": "",
       	"subscribers": {
       		"https": true,
       		"redis": true
       	},
       	"apiOriginAllow": {
                       "allowCors": true,
                       "allowOrigin": "https://crm3.alif.tj",
                       "allowMethods": "GET, POST",
                       "allowHeaders": "Origin, Content-Type, X-Auth-Token, X-Requested-With, Accept, Authorization, X-CSRF-TOKEN, X-Socket-Id"
               }
       }
       

## Code overview
### Folders

- `app/Modules` - Contains all the domains
- `app/Interfaces` - Contains all the interfaces that are used by the all domains
- `app/Requests` - Contains all the requests that are used by the all domains
- `app/Services` - Contains all the services that are used by the all domains
- `app/Http/Controllers/ModulesDataListMediator.php` - Contains all the methods of domains' list
- `app/Http/Controllers/Admin` - Contains all the admin controllers
- `app/Http/Controllers/Person` - Contains all the person controllers
- `app/Modules/Domain/Person` - Contains all the domain logic for person controllers
- `app/Modules/Domain/Admin` - Contains all the domain logic for admin controllers
- `config` - Contains all the application configuration files
- `database/factories` - Contains the model factory for all the models
- `database/migrations` - Contains all the database migrations
- `database/seeds` - Contains the database seeder
- `routes` - Contains all the api routes defined in api.php file
- `tests` - Contains all the application tests
- `tests/Api` - Contains all the api tests
- `tests/Integration` - Contains all the integration tests
- `tests/Unit` - Contains all the unit tests

## Features
### Services

 - `https://ivr.alif.tj` - IVR service
 - `https://sms.aliftech.net` - SMS center service
 - `https://crm3.alif.tj` - CRM service
 
## Testing
### Installation

Before any tests could be run you need to install sqlite3 and php-sqlite3 on your OS
    
    sudo apt install sqlite3
    sudo apt-get install php-sqlite3
    
Run tests

    php artisan test
    
## Environment variables

### General requirements

    BROADCAST_DRIVER=redis
    CACHE_DRIVER=redis
    QUEUE_CONNECTION=redis
    SESSION_DRIVER=file
    SESSION_LIFETIME=120
    
    REDIS_HOST=127.0.0.1
    REDIS_PASSWORD=null
    REDIS_PORT=6379
    
    MAIL_DRIVER=smtp
    MAIL_HOST=smtp.gmail.com
    MAIL_PORT=587
    MAIL_USERNAME=queue.alif@gmail.com
    MAIL_PASSWORD=Q@l!f2018
    MAIL_ENCRYPTION=tls
    
    PUSHER_APP_ID=
    PUSHER_APP_KEY=
    PUSHER_APP_SECRET=
    PUSHER_APP_CLUSTER=mt1
    
    MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
    MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
    
    ASTERISK_TRUNK_NUMBER=32423455
    ASTERISK_TRUNK_NAME=OUT
    ASTERISK_CONTEXT=queue
    STASIS_APPLICATION_NAME=queueAri
    
    JOB_ADD_TIME=20
    API_TOKEN='tJpftnqoExF9svuGwLsijLnYqlns6qc3Q7SGprc9CplgP0lyuf69fStFsb2J'
    API_APPLICATION_NAME=queueDushanbe
    
    SMS_CENTER_TOKEN=bc658a845e7e6b82149a887ee15afa662612d8fc0e2d824b198d99e0753f8a8f
    CRM_SEARCH_API_URL=https://apicrm3.alif.tj/api_service/queue/v1/search-client
    CRM_SEARCH_API_TOKEN=97ebad8dd4449d16de1d984f7701889176782g3igjhghjg823t523g5i2
    
    
    CRM_URL=https://apicrm3.alif.tj
    CRM_API_TOKEN=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.xSpJPE_SK_fB6yMDDvOCd_8_7AeM2tUye8SPkMcs4q8
    
    IVR_URL=https://ivr.alif.tj
    IVR_SECRET_TOKEN=tJpftnqoExF9svuGwLsijLnYqlns6qc3Q7SGprc9CplgP0lyuf69fStFsb2J
    
    JWT_SECRET=cnjVS7uFyTMzgcRfDBetH4eSafwcghEAnTiSqqYYKnTqRnCEMAG0hCaclS73eeUC
    TELESCOPE_ENABLED=false
    
    SENTRY_LARAVEL_DSN=https://3d23cff46dde4c4f9bef20f96416aec4@sentry.alif.tj/5
    SENTRY_TRACES_SAMPLE_RATE=1
    
###  Production requirements

    APP_URL=https://nq.alif.tj
    
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=queue
    DB_USERNAME=root
    DB_PASSWORD=my$ql@dm!n2o19
    
## Deployment process
Deployment process consists of 3 key points([Deployer tasks list](https://github.com/deployphp/deployer/blob/master/docs/recipe/laravel.md)):
    
 - [Php deployer library](https://deployer.org/).
 - [GitHub Actions Runner](https://github.com/actions/runner).
 - [Docker Action](https://github.com/myoung34/docker-github-actions-runner).
 
###  Production/Test servers 
To run github-runner through docker run the next command:

    sudo docker run -d --restart always --network host --name github-runner -e REPO_URL=https://github.com/alifcapital/queue.backend -e ACCESS_TOKEN=YOUR_GITHUB_ACTION_TOKEN myoung34/github-runner:latest
