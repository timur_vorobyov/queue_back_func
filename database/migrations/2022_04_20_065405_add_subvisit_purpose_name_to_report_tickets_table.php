<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSubvisitPurposeNameToReportTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_tickets', function (Blueprint $table) {
            $table->string('sub_visit_purpose_name')->after('visit_purpose_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_tickets', function (Blueprint $table) {
            $table->dropColumn('sub_visit_purpose_name');
        });
    }
}
