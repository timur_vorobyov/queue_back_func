<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubVisitPurposesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_visit_purposes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('visit_purpose_id')->default(0);
            $table->string('name');
            $table->timestamps();
            $table->foreign('visit_purpose_id')
                ->references('id')
                ->on('visit_purposes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_visit_purposes');
    }
}
