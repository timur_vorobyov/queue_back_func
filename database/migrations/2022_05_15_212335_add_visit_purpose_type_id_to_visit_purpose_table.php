<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVisitPurposeTypeIdToVisitPurposeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visit_purposes', function (Blueprint $table) {
            $table->unsignedBigInteger('visit_purpose_type_id')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visit_purposes', function (Blueprint $table) {
            $table->dropColumn('visit_purpose_type_id');
        });
    }
}
