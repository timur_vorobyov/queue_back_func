<?php

namespace Database\Factories;

use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory;

class VisitPurposeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = VisitPurpose::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'series' => $this->faker->word,
            'tj_message' => $this->faker->word,
            'ru_message' => $this->faker->word,
            'is_prioratised' => false
        ];
    }
}
