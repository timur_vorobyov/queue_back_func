<?php
namespace Deployer;


require 'vendor/deployer/deployer/recipe/laravel.php';
require 'contrib/rsync.php';

set('application', 'queue.backend');
set('repository','git@github.com:alifcapital/queue.backend.git');
set('composer_action','install --ignore-platform-reqs');
set('keep_releases', 2);
set('ssh_multiplexing', true);
set('rsync_src', function () {
    return DIR;
});
set('shared_dirs', ['storage']);
set('shared_files', ['laravel-echo-server.json', '.env']);

add('rsync', [
    'exclude' => [
        '/.git',
        '/.env',
        '/storage/',
        '/vendor/',
        '/node_modules/',
        '/.github',
        '/deploy.php',
    ],
]);

task('deploy:secrets', function () {
    file_put_contents(DIR . '/.env', getenv('DOT_ENV'));
    upload('.env', get('deploy_path') . '/shared');
});

host('nq.alif.tj')
    ->set('labels', ['stage' => 'staging'])
    ->set('roles', 'app')
    ->set('branch', 'develop')
    ->setDeployPath('/var/www/queue.backend')
    ->setHostname('192.168.13.178')
    ->setRemoteUser('timur');

host('apinq.alif.tj')
    ->set('labels', ['stage' => 'production'])
    ->set('roles', 'app')
    ->set('branch', 'master')
    ->setDeployPath('/var/www/queue.backend')
    ->setHostname('192.168.15.133')
    ->setRemoteUser('timur');





desc('Deploy the application');

task('deploy staging', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'rsync',
    'deploy:secrets',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);


after('deploy:failed', 'deploy:unlock');
