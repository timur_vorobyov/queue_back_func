<?php

namespace Tests\Api\Admin\SuperAdmin\Town;

use App\Modules\Town\Town;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CRUDTownUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $townFactory;
    private Town $town;

    protected function setUp(): void
    {
        parent::setUp();
        $this->townFactory = Town::factory();
        $this->town = app(Town::class);
    }

    public function testASuperAdminCanCreateTown()
    {
        $this->withoutExceptionHandling();

        $attributes = [
            "name" => $this->faker->word,
        ];

        $this->post(route("super-admin.storeTown", $attributes))->assertStatus(
            200,
        );
        $this->assertDatabaseHas("towns", $attributes);
    }

    public function testASuperAdminCanUpdateTown()
    {
        $this->withoutExceptionHandling();

        $town = $this->townFactory->create([
            "name" => "Dushanbe",
        ]);

        $attributes = [
            "id" => $town->id,
            "name" => "Xudjand",
        ];

        $response = $this->put(
            "/api/super-admin/towns/" . $town->id,
            $attributes,
        )->assertStatus(200);

        $updatedTown = $this->town
            ::find($town->id)
            ->toDomainEntity()
            ->superAdminJsonSerialize();

        $this->assertEquals($updatedTown, $attributes);
        $this->assertEquals("Town is updated", $response["message"]);
    }

    public function testASuperAdminCanDeleteTown()
    {
        $town = $this->townFactory->create();
        $this->delete("/api/super-admin/town/" . $town->id);
        $this->assertDatabaseMissing("towns", ["id" => $town->id]);
    }

    public function testASuperAdminCanGetTownsList()
    {
        $towns = $this->townFactory
            ->count(5)
            ->create([
                "name" => "Dushanbe",
            ])
            ->map(function ($town) {
                return $town->toDomainEntity()->superAdminJsonSerialize();
            });

        $this->get(route("super-admin.getPaginatedTownsList"))
            ->assertStatus(200)
            ->assertJson($towns->toArray())
            ->assertJsonStructure([
                "*" => ["id", "name"],
            ]);
    }
}
