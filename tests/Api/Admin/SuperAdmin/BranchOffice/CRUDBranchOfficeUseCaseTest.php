<?php

namespace Tests\Api\Admin\SuperAdmin\BranchOffice;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Town\Town;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class CRUDBranchOfficeUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $branchOfficeFactory,
        $townFactory,
        $officeFactory,
        $visitPurposeFactory;
    private BranchOffice $branchOffice;

    protected function setUp(): void
    {
        parent::setUp();
        $this->branchOfficeFactory = BranchOffice::factory();
        $this->townFactory = Town::factory();
        $this->visitPurposeFactory = VisitPurpose::factory();
        $this->branchOffice = app(BranchOffice::class);
    }

    public function testASuperAdminCanCreateBranchOffice()
    {
        $this->withoutExceptionHandling();

        $visitPurposesIds = $this->visitPurposeFactory
            ->count(3)
            ->create()
            ->pluck("id")
            ->all();

        $attributes = [
            "name" => $this->faker->unique()->word(),
            "initial" => $this->faker->unique()->randomLetter,
            "timer" => $this->faker->numberBetween($min = 1000, $max = 5000),
            "townId" => Town::factory()->create()->id,
            "visitPurposesIds" => $visitPurposesIds,
        ];

        $requestStub = new CRUDBranchOfficeRequestStub($attributes);
        $modifiedAttributes = $requestStub->toDatabaseFormat();
        $pivotTablesIds = DB::table("branch_office_visit_purpose")
            ->whereIn("visit_purpose_id", $visitPurposesIds)
            ->pluck("id")
            ->all();

        $this->post(
            route("super-admin.storeBranchOffice", $attributes),
        )->assertStatus(200);
        $this->assertDatabaseHas("branch_offices", $modifiedAttributes);
        foreach ($pivotTablesIds as $pivotTableId) {
            $this->assertDatabaseHas("branch_office_visit_purpose", [
                "id" => $pivotTableId,
            ]);
        }
    }

    public function testASuperAdminCanUpdateBranchOffice()
    {
        $this->withoutExceptionHandling();

        $branchOffice = $this->branchOfficeFactory
            ->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])
            ->create([
                "town_id" => 1,
                "name" => "lan",
            ]);
        $town = $this->townFactory->create(["id" => 2]);

        $visitPurposesIds = $this->visitPurposeFactory
            ->count(3)
            ->create()
            ->pluck("id")
            ->all();

        $attributes = [
            "id" => $branchOffice->id,
            "initial" => $branchOffice->initial,
            "townId" => $town->id,
            "name" => "dic",
            "timer" => $this->faker->numberBetween($min = 1000, $max = 5000),
            "visitPurposesIds" => $visitPurposesIds,
        ];

        $response = $this->put(
            "/api/super-admin/branch-offices/" . $branchOffice->id,
            $attributes,
        )->assertStatus(200);

        $comparedBranchOffice = [
            "id" => $attributes["id"],
            "initial" => $attributes["initial"],
            "townId" => $attributes["townId"],
            "branchOfficeName" => $attributes["name"],
        ];

        $updatedBranchOffice = $this->branchOffice
            ::find($branchOffice->id)
            ->toDomainEntity()
            ->jsonSerialize();

        array_pop($updatedBranchOffice);
        $this->assertEquals($updatedBranchOffice, $comparedBranchOffice);
        $this->assertEquals("Branch office is updated", $response["message"]);
        foreach ($visitPurposesIds as $visitPurposesId) {
            $this->assertDatabaseHas("branch_office_visit_purpose", [
                "id" => $visitPurposesId,
            ]);
        }
    }

    public function testASuperAdminCanDeleteBranchOffice()
    {
        $branchOffice = $this->branchOfficeFactory->create();
        $this->delete("/api/super-admin/branch-office/" . $branchOffice->id);
        $this->assertDatabaseMissing("branch_offices", [
            "id" => $branchOffice->id,
        ]);
    }

    public function testASuperAdminCanGetBranchOfficesList()
    {
        $branchOffices = $this->branchOfficeFactory
            ->count(5)
            ->create()
            ->map(function ($branchOffice) {
                return $branchOffice->toDomainEntity()->jsonSerialize();
            });

        $this->get(route("super-admin.getPaginatedBranchOfficesList"))
            ->assertStatus(200)
            ->assertJson($branchOffices->toArray())
            ->assertJsonStructure([
                "*" => ["id", "townId", "initial", "timer"],
            ]);
    }
}
