<?php

namespace Tests\Api\Admin\SuperAdmin\Permission;

use App\Modules\Permission\Permission;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CRUDPermissionUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $permissionFactory;
    private Permission $permission;

    protected function setUp(): void
    {
        parent::setUp();
        $this->permission = app(Permission::class);
        $this->permissionFactory = Permission::factory();
    }

    public function testASuperAdminCanCreatePermission()
    {
        $this->withoutExceptionHandling();

        $attributes = [
            "name" => $this->faker->word,
            "guardName" => "web",
        ];

        $requestStub = new CRUDPermissionRequestStub($attributes);
        $modifiedAttributes = $requestStub->toDatabaseFormat();

        $this->post(
            route("super-admin.storePermission", $attributes),
        )->assertStatus(200);
        $this->assertDatabaseHas("permissions", $modifiedAttributes);
    }

    public function testASuperAdminCanUpdatePermission()
    {
        $this->withoutExceptionHandling();
        $permission = $this->permissionFactory->create();

        $attributes = [
            "id" => $permission->id,
            "name" => $this->faker->word,
            "guardName" => "web",
        ];

        $response = $this->put(
            "/api/super-admin/permissions/" . $permission->id,
            $attributes,
        )->assertStatus(200);

        $updatedPermission = $this->permission
            ::find($permission->id)
            ->toDomainEntity()
            ->superAdminJsonSerialize();

        $this->assertEquals($updatedPermission, $attributes);
        $this->assertEquals("Permission is updated", $response["message"]);
    }

    public function testASuperAdminCanDeletePermission()
    {
        $permission = $this->permissionFactory->create();
        $this->delete("/api/super-admin/permission/" . $permission->id);
        $this->assertDatabaseMissing("permissions", ["id" => $permission->id]);
    }

    public function testASuperAdminCanGetPermissionsList()
    {
        $permissions = $this->permissionFactory
            ->count(3)
            ->create()
            ->map(function ($permission) {
                return $permission->toDomainEntity()->superAdminJsonSerialize();
            });

        $this->get(route("super-admin.getPaginatedPermissionsList"))
            ->assertStatus(200)
            ->assertJson($permissions->toArray())
            ->assertJsonStructure([
                "*" => ["id", "name", "guardName"],
            ]);
    }
}
