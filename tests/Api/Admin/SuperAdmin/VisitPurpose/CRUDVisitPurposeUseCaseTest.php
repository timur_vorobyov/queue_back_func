<?php

namespace Tests\Api\Admin\SuperAdmin\VisitPurpose;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Service\Service;
use App\Modules\VisitPurpose\VisitPurpose;
use Database\Factories\ServiceFactory;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class CRUDVisitPurposeUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $serviceFactory,
        $visitPurposeFactory,
        $branchOfficeFactory;
    private VisitPurpose $visitPurpose;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serviceFactory = Service::factory();
        $this->visitPurposeFactory = VisitPurpose::factory();
        $this->branchOfficeFactory = BranchOffice::factory();
        $this->visitPurpose = app(VisitPurpose::class);
    }

    public function testASuperAdminCanCreateVisitPurpose()
    {
        $this->withoutExceptionHandling();

        $servicesIds = $this->serviceFactory
            ->count(3)
            ->create()
            ->pluck("id")
            ->all();

        $attributes = [
            "servicesIds" => $servicesIds,
            "name" => $this->faker->word,
            "series" => $this->faker->word,
            "tjMessage" => $this->faker->word,
            "ruMessage" => $this->faker->word,
            "isPrioratised" => false
        ];

        $requestStub = new CRUDVisitPurposeRequestStub($attributes);
        $modifiedAttributes = $requestStub->toDatabaseFormat();

        $this->post(
            route("super-admin.storeVisitPurpose", $attributes),
        )->assertStatus(200);

        $pivotTablesIds = DB::table("service_visit_purpose")
            ->whereIn("service_id", $servicesIds)
            ->pluck("id")
            ->all();

        $this->assertDatabaseHas("visit_purposes", $modifiedAttributes);
        foreach ($pivotTablesIds as $pivotTableId) {
            $this->assertDatabaseHas("service_visit_purpose", [
                "id" => $pivotTableId,
            ]);
        }
    }

    public function testASuperAdminCanUpdateVisitPurpose()
    {
        $this->withoutExceptionHandling();

        $visitPurpose = $this->visitPurposeFactory
            ->hasAttached($this->serviceFactory, ["id" => 1])
            ->create(["id" => 1])
            ->toDomainEntity()
            ->jsonSerialize();

        $servicesIds = $this->serviceFactory
            ->count(3)
            ->create()
            ->pluck("id")
            ->all();

        $attributes = [
            "id" => $visitPurpose["id"],
            "servicesIds" => $servicesIds,
            "name" => $this->faker->word,
            "series" => $this->faker->word,
            "tjMessage" => $this->faker->word,
            "ruMessage" => $this->faker->word,
            "isPrioratised" => false
        ];

        $response = $this->put(
            "/api/super-admin/visit-purposes/" . $visitPurpose["id"],
            $attributes,
        )->assertStatus(200);

        $updatedVisitPurpose = $this->visitPurpose
            ::find(1)
            ->toDomainEntity()
            ->jsonSerialize();

        foreach ($servicesIds as $serviceId) {
            $this->assertDatabaseHas("service_visit_purpose", [
                "id" => $serviceId,
            ]);
        }

        $this->assertEquals($updatedVisitPurpose["name"], $attributes["name"]);
        $this->assertEquals(
            $updatedVisitPurpose["series"],
            $attributes["series"],
        );
        $this->assertEquals("Visit purpose is updated", $response["message"]);
    }

    public function testASuperAdminCanDeleteVisitPurpose()
    {
        $visitPurpose = $this->visitPurposeFactory->create();
        $this->delete("/api/super-admin/visit-purpose/" . $visitPurpose->id);
        $this->assertDatabaseMissing("visit_purposes", [
            "id" => $visitPurpose->id,
        ]);
    }

    public function testASuperAdminCanGetPaginatedVisitPurposesListFilteredBySearchExpresion()
    {
        $limit = 5;
        $currentPage = 2;
        $totalPages = 2;
        $totalRecords = 8;

        $this->visitPurposeFactory->count(5)->create([
            "name" => "len",
        ]);

        $this->visitPurposeFactory->count(8)->create([
            "name" => "John",
        ]);

        $attributes = [
            "searchExpression" => "John",
            "page" => $currentPage,
            "limit" => $limit,
        ];

        $response = $this->get(
            route("super-admin.getPaginatedVisitPurposesList", $attributes),
        )->assertStatus(200);

        $responseArray = json_decode($response->content());
        $this->assertEquals(
            $responseArray->pagination->currentPage,
            $currentPage,
        );
        $this->assertEquals(
            $responseArray->pagination->totalPages,
            $totalPages,
        );
        $this->assertEquals(
            $responseArray->pagination->totalRecords,
            $totalRecords,
        );
        $this->assertEquals($responseArray->pagination->pageLimit, $limit);
    }

    public function testASuperAdminCanGetPaginatedAllVisitPurposesList()
    {
        $this->visitPurposeFactory->count(5)->create([
            "name" => "len",
        ]);

        $this->visitPurposeFactory->count(8)->create([
            "name" => "John",
        ]);

        $limit = 5;
        $currentPage = 1;
        $totalPages = 3;
        $totalRecords = 13;

        $attributes = [
            "limit" => $limit,
        ];

        $response = $this->get(
            route("super-admin.getPaginatedVisitPurposesList", $attributes),
        )->assertStatus(200);

        $responseArray = json_decode($response->content());
        $this->assertEquals(
            $responseArray->pagination->currentPage,
            $currentPage,
        );
        $this->assertEquals(
            $responseArray->pagination->totalPages,
            $totalPages,
        );
        $this->assertEquals(
            $responseArray->pagination->totalRecords,
            $totalRecords,
        );
        $this->assertEquals($responseArray->pagination->pageLimit, $limit);
    }

    public function testASuperAdminCanGetPaginatedVisitPurposesListFilteredByBranchOffice()
    {
        $this->visitPurposeFactory
            ->count(5)
            ->hasAttached($this->branchOfficeFactory, ["branch_office_id" => 1])
            ->create();

        $this->visitPurposeFactory
            ->count(8)
            ->hasAttached($this->branchOfficeFactory, ["branch_office_id" => 2])
            ->create();

        $limit = 5;
        $currentPage = 2;
        $totalPages = 2;
        $totalRecords = 8;

        $attributes = [
            "branchOfficeId" => "2",
            "page" => $currentPage,
            "limit" => $limit,
        ];

        $response = $this->get(
            route("super-admin.getPaginatedVisitPurposesList", $attributes),
        )->assertStatus(200);

        $responseArray = json_decode($response->content());
        $this->assertEquals(
            $responseArray->pagination->currentPage,
            $currentPage,
        );
        $this->assertEquals(
            $responseArray->pagination->totalPages,
            $totalPages,
        );
        $this->assertEquals(
            $responseArray->pagination->totalRecords,
            $totalRecords,
        );
        $this->assertEquals($responseArray->pagination->pageLimit, $limit);
    }

    public function testASuperAdminCanGetPaginatedVisitPurposesListFilteredByBranchOfficeAndSearchExpression()
    {
        $this->visitPurposeFactory
            ->count(5)
            ->hasAttached($this->branchOfficeFactory, ["branch_office_id" => 1])
            ->create([
                "name" => "Alex",
            ]);

        $this->visitPurposeFactory
            ->count(8)
            ->hasAttached($this->branchOfficeFactory, ["branch_office_id" => 1])
            ->create([
                "name" => "John",
            ]);

        $this->visitPurposeFactory
            ->count(8)
            ->hasAttached($this->branchOfficeFactory, ["branch_office_id" => 2])
            ->create([
                "name" => "Marcel",
            ]);

        $limit = 5;
        $currentPage = 1;
        $totalPages = 3;
        $totalRecords = 13;

        $attributes = [
            "branchOfficeId" => "1",
            "name" => "Alex",
            "page" => $currentPage,
            "limit" => $limit,
        ];

        $response = $this->get(
            route("super-admin.getPaginatedVisitPurposesList", $attributes),
        )->assertStatus(200);

        $responseArray = json_decode($response->content());
        $this->assertEquals(
            $responseArray->pagination->currentPage,
            $currentPage,
        );
        $this->assertEquals(
            $responseArray->pagination->totalPages,
            $totalPages,
        );
        $this->assertEquals(
            $responseArray->pagination->totalRecords,
            $totalRecords,
        );
        $this->assertEquals($responseArray->pagination->pageLimit, $limit);
    }
}
