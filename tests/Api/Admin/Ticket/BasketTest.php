<?php

namespace Tests\Api\Admin\Ticket;

use App\Modules\Status\Status;
use App\Modules\Ticket\Ticket;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BasketTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private Ticket $ticket;
    private FactoryObject $ticketFactory, $statusFactory, $visitPurposeFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->ticketFactory = Ticket::factory();
        $this->ticket = app(Ticket::class);
        $this->statusFactory = Status::factory();
        $this->visitPurposeFactory = VisitPurpose::factory();
    }

    public function testGetDeclinedTicketsList()
    {
        $this->withoutExceptionHandling();

        $status = $this->statusFactory->create([
            "id" => 4,
            "name" => "declined",
        ]);
        $visitPurpose = $this->visitPurposeFactory->create([
            "id" => 1
        ]);
        $this->ticketFactory
            ->count(2)
            ->create([
                "visit_purpose_id" => $visitPurpose->id,
                "status_id" => $status->id,
                "branch_office_id" => 1,
            ]);

        $attributes = [
            "statusName" => $status->name,
            "branchOfficeId" => 1,
        ];

        $gotTickets = $this->ticket::with('visitPurpose','branchOffice')->get()->map(function ($ticket) {
            return $ticket->toDomainEntity()->adminJsonSerialize();
        })->toArray();

        $this->get(route("hall-admin.getDeclinedTicketsList", $attributes))
            ->assertStatus(200)
            ->assertJson($gotTickets)
            ->assertJsonStructure([
                "*" => [
                    "id",
                    "customerPhoneNumber",
                    "createdAt",
                    "visitPurposeName",
                    "ticketFullNumber",
                ],
            ]);
    }

    public function testRecoverTicket()
    {
        $this->withoutExceptionHandling();

        $status = $this->statusFactory->create([
            "id" => 4,
            "name" => "declined",
        ]);
        $desiredStatus = $this->statusFactory->create([
            "id" => 1,
            "name" => "new",
        ]);

        $ticket = $this->ticketFactory->create([
            "status_id" => $status->id,
        ]);

        $response = $this->put(
            "/api/hall-admin/recover-ticket/" . $ticket->id,
        )->assertStatus(200);

        $recoveredTicket = $this->ticket::find($ticket->id);

        $this->assertEquals($recoveredTicket->status_id, $desiredStatus->id);
        $this->assertEquals("TicketController is recovered", $response["message"]);
    }
}
