<?php

namespace Tests\Api\Person\User\UseCases;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Role\Role;
use App\Modules\Ticket\Ticket;
use App\Modules\User\User;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class UpdateUserSettingsUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $userFactory,
        $visitPurposeFactory,
        $branchOfficeFactory,
        $ticketFactory,
        $roleFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userFactory = User::factory();
        $this->visitPurposeFactory = VisitPurpose::factory();
        $this->branchOfficeFactory = BranchOffice::factory();
        $this->roleFactory = Role::factory();
        $this->ticketFactory = Ticket::factory();
    }

    public function testUserWorkDataIsUpdated()
    {
        $savedVisitPurposeId = $this->visitPurposeFactory->create(["id" => 1])
            ->id;
        $desiredVisitPurposesIds = $this->visitPurposeFactory
            ->count(3)
            ->create()
            ->pluck("id")
            ->all();;
        $branchOffice = $this->branchOfficeFactory->create();
        $savedUser = $this->userFactory->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => $savedVisitPurposeId])->create([
            "branch_office_id" => 1,
            "desk_number" => "1",
        ]);

        $role = $this->roleFactory->create();
        $savedUser->assignRole($role->name);
        $savedUser = $savedUser->toDomainEntity();

        $attributes = [
            "userId" => $savedUser->getId(),
            "branchOfficeId" => $branchOffice->id,
            "visitPurposesIds" => $desiredVisitPurposesIds,
            "deskNumber" => "2",
        ];

        $response = $this->post(route("updateWorkData", $attributes))
            ->assertStatus(200)
            ->assertJsonStructure(["userData", "message"]);

        $this->assertEquals(
            "USER'S_SETTINGS_ARE_UPDATED",
            $response["message"],
        );
        $this->assertEquals(
            $attributes["deskNumber"],
            $response["userData"]["deskNumber"],
        );
        $this->assertEquals(
            $branchOffice->id,
            $response["userData"]["branchOfficeId"],
        );

        foreach ($desiredVisitPurposesIds as $visitPurposesId) {
            $this->assertDatabaseHas("user_visit_purpose", [
                "visit_purpose_id" => $visitPurposesId,
            ]);
        }
    }

}
