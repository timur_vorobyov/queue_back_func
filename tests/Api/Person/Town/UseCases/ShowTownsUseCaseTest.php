<?php

namespace Tests\Api\Person\Town\UseCases;

use App\Modules\Town\Town;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowTownsUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $townFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->townFactory = Town::factory();
    }

    public function testTownsAreListedCorrectly()
    {
        $this->withoutExceptionHandling();

        $towns = $this->townFactory->count(2)->create();

        $this->get(route("showTowns"))
            ->assertStatus(200)
            ->assertJson($towns->toArray())
            ->assertJsonStructure([
                "*" => ["id", "name"],
            ]);
    }
}
