<?php

namespace Tests\Api\Person\Ticket\UseCases;

use App\Modules\Status\Status;
use App\Modules\Ticket\Person\Events\TicketAcceptEvent;
use App\Modules\Ticket\Ticket;
use App\Modules\User\User;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Testing\Fakes\BusFake;
use Illuminate\Support\Testing\Fakes\EventFake;
use Tests\TestCase;

class DeclineTicketUseCAseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private EventFake $eventFake;
    private BusFake $busFake;
    private FactoryObject $userFactory,
        $ticketFactory,
        $visitPurposeFactory,
        $statusFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->eventFake = Event::fake();
        $this->visitPurposeFactory = VisitPurpose::factory();
        $this->busFake = Bus::fake();
        $this->userFactory = User::factory();
        $this->ticketFactory = Ticket::factory();
        $this->statusFactory = Status::factory();
    }

    public function testAUserCanNotDeclineTicketAsVisitPurposesWasNotChosen()
    {
        $this->withoutExceptionHandling();

        $user = $this->userFactory->create();

        $ticket = $this->ticketFactory->create([
            "status_id" => 5,
            "user_id" => $user->id,
        ]);

        $attributes = [
            "userId" => $user->id,
            "ticketId" => $ticket->id,
        ];

        $response = $this->put(
            route("declineTicket", $attributes),
        )->assertStatus(200);

        $this->assertEquals("The visit goal wasn't chosen", $response["data"]);
    }

    public function testAUserCanNotDeclineTicketAsDeskNumberWasNotChosen()
    {
        $this->withoutExceptionHandling();

        $user = $this->userFactory->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])->create(["desk_number" => null]);

        $ticket = $this->ticketFactory->create([
            "status_id" => 5,
            "user_id" => $user->id,
        ]);

        $attributes = [
            "userId" => $user->id,
            "ticketId" => $ticket->id,
        ];

        $response = $this->put(
            route("declineTicket", $attributes),
        )->assertStatus(200);

        $this->assertEquals(
            "The number of desk wasn't chosen",
            $response["data"],
        );
    }

    public function testAUserCanDeclineTicket()
    {
        $this->withoutExceptionHandling();
        $this->statusFactory->create(["id" => 2, "name" => "invited"]);
        $this->statusFactory->create(["id" => 5, "name" => "declined"]);
        $user = $this->userFactory->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])->create();

        $ticket = $this->ticketFactory->create([
            "status_id" => 2,
            "user_id" => $user->id,
            "visit_purpose_id" => 1,
            "branch_office_id" => $user->branch_office_id,
        ]);

        $attributes = [
            "userId" => $user->id,
            "ticketId" => $ticket->id,
        ];

        $response = $this->put(
            route("declineTicket", $attributes),
        )->assertStatus(200);

        $this->eventFake->assertDispatched(TicketAcceptEvent::class, 2);
        $this->assertEquals("Client is declined", $response["data"]);
    }
}
