<?php

namespace Tests\Api\Person\Ticket\UseCases;

use App\Modules\Status\Status;
use App\Modules\Ticket\Person\Events\TicketAcceptEvent;
use App\Modules\Ticket\Person\Events\TicketPendingEvent;
use App\Modules\Ticket\Person\Jobs\SmsSenderJob;
use App\Modules\Ticket\Ticket;
use App\Modules\User\User;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Testing\Fakes\BusFake;
use Illuminate\Support\Testing\Fakes\EventFake;
use Tests\TestCase;

class AcceptTicketUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private EventFake $eventFake;
    private BusFake $busFake;
    private FactoryObject $userFactory,
        $visitPurposeFactory,
        $ticketFactory,
        $statusFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->eventFake = Event::fake();
        $this->busFake = Bus::fake();
        $this->userFactory = User::factory();
        $this->visitPurposeFactory = VisitPurpose::factory();
        $this->ticketFactory = Ticket::factory();
        $this->statusFactory = Status::factory();
    }

    public function testAUserCanNotAcceptTicketAsVisitPurposesWasNotChosen()
    {
        $this->withoutExceptionHandling();

        $user = $this->userFactory
            ->create();
        $ticket = $this->ticketFactory->create(["status_id" => 2, "user_id" => null]);

        $attributes = [
            "userId" => $user->id,
            "ticketId" => $ticket->id
        ];

        $response = $this->put(
            route("acceptTicket", $attributes),
        )->assertStatus(200);

        $this->assertEquals("The visit goal wasn't chosen", $response["data"]);
    }

    public function testAUserCanNotAcceptTicketAsDeskNumberWasNotChosen()
    {
        $this->withoutExceptionHandling();

        $user = $this->userFactory->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])->create(["desk_number" => null]);
        $ticket = $this->ticketFactory->create(["status_id" => 2, "user_id" => null]);

        $attributes = [
            "userId" => $user->id,
            "ticketId" => $ticket->id
        ];

        $response = $this->put(
            route("acceptTicket", $attributes),
        )->assertStatus(200);

        $this->assertEquals(
            "The number of desk wasn't chosen",
            $response["data"],
        );
    }

    public function testAUserCanAcceptTicket()
    {
        $this->withoutExceptionHandling();
        $this->statusFactory->create(["id" => 1, "name" => "new"]);
        $this->statusFactory->create(["id" => 2, "name" => "invited"]);

        $user = $this->userFactory->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])->create();
        $ticket = $this->ticketFactory->create([
            "status_id" => 1,
            "user_id" => $user->id,
            "visit_purpose_id" => 1,
            "branch_office_id" => $user->branch_office_id,
        ]);
        $attributes = [
            "userId" => $user->id,
            "ticketId" => $ticket->id
        ];

        $response = $this->put(
            route("acceptTicket", $attributes),
        )->assertStatus(200);
        $this->eventFake->assertDispatched(TicketAcceptEvent::class);
        $this->eventFake->assertDispatched(TicketPendingEvent::class);
        $this->busFake->assertDispatched(SmsSenderJob::class);
        $this->assertEquals("You have accepted client", $response["data"]);
    }
}
