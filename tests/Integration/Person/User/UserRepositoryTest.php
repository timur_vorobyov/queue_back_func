<?php

namespace Tests\Integration\Person\User;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\User\Person\Interfaces\IUserRepository;
use App\Modules\User\User;
use App\Modules\User\Person\UserRepository;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;

class UserRepositoryTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private IUserRepository $userRepository;
    private FactoryObject $userFactory,
        $visitPurposeFactory,
        $branchOfficeFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = app(UserRepository::class);
        $this->userFactory = User::factory();
        $this->visitPurposeFactory = VisitPurpose::factory();
        $this->branchOfficeFactory = BranchOffice::factory();
    }

    public function testLogin()
    {
        $user = $this->userFactory->create();
        $this->assertIsBool(
            $this->userRepository->login($user->email, "123456789"),
        );
    }

    public function testGetUserByEmail()
    {
        $user = $this->userFactory->create()::with('branchOffice')->first()->toDomainEntity();
        $this->assertEquals(
            $user,
            $this->userRepository->getUserByEmail($user->getEmail()),
        );
    }

    public function testUpdateWorkData()
    {
        $savedVisitPurposeId = $this->visitPurposeFactory->create(["id" => 1])
            ->id;
        $desiredVisitPurposeId = $this->visitPurposeFactory->create(["id" => 2])
            ->id;
        $branchOffice = $this->branchOfficeFactory->create();
        $savedUser = $this->userFactory
            ->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => $savedVisitPurposeId])
            ->create([
                "branch_office_id" => $branchOffice->id,
                "desk_number" => "1" . $branchOffice->initial,
            ])
            ->toDomainEntity();

        $desiredUser = $this->userRepository->updateSettingsData([
            "userId" => $savedUser->getId(),
            "branchOfficeId" => $branchOffice->id,
            "visitPurposesIds" => [$desiredVisitPurposeId],
            "deskNumber" => "2",
            "userIdWithSameDeskNumber" => null
        ]);

        $this->assertEquals($desiredUser->getId(), $savedUser->getId());
        $this->assertEquals(
            $desiredUser->getDeskNumber(),
            2 . $branchOffice->initial,
        );
        $this->assertEquals(
            $desiredUser->getBranchOfficeId(),
            $branchOffice->id,
        );
    }
}
