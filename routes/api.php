<?php

use App\Http\Controllers\Admin\HallAdmin\UserController as HallAdminUserController;
use App\Http\Controllers\Admin\BasketController as AdminBasketController;
use App\Http\Controllers\Admin\TicketController as AdminTicketController;

use App\Http\Controllers\Admin\SuperAdmin\PermissionController as SuperAdminPermissionController;
use App\Http\Controllers\Admin\SuperAdmin\RoleController as SuperAdminRoleController;
use App\Http\Controllers\Admin\SuperAdmin\TicketController as SuperAdminTicketController;
use App\Http\Controllers\Admin\SuperAdmin\ReportController as SuperAdminReportController;
use App\Http\Controllers\Admin\SuperAdmin\UserController as SuperAdminUserController;
use App\Http\Controllers\Admin\SuperAdmin\TownController as SuperAdminTownController;
use App\Http\Controllers\Admin\SuperAdmin\BranchOfficeController as SuperAdminBranchOfficeController;
use App\Http\Controllers\Admin\SuperAdmin\ServiceController as SuperAdminServiceController;
use App\Http\Controllers\Admin\SuperAdmin\VisitPurposeController as SuperAdminVisitPurposeController;
use App\Http\Controllers\Admin\SuperAdmin\SubVisitPurposeController as SuperAdminSubVisitPurposeController;
use App\Http\Controllers\Admin\SuperAdmin\VisitPurposeTypeController as SuperAdminVisitPurposeTypeController;

use App\Http\Controllers\ModulesDataListMediator;
use App\Http\Controllers\Person\BranchOfficeController as PersonBranchOfficeController;
use App\Http\Controllers\Person\ServiceController as PersonServiceController;
use App\Http\Controllers\Person\TownController as PersonTownController;
use App\Http\Controllers\Person\TicketController;
use App\Http\Controllers\Person\UserController as PersonUserController;
use App\Http\Controllers\Person\VisitPurposeController as PersonVisitPurposeController;
use App\Http\Controllers\Person\SubVisitPurposeController as PersonSubVisitPurposeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('super-admin')->group(function () {
    /** User **/
    Route::get('paginated-users', [SuperAdminUserController::class, 'getPaginatedUsersList'])->name("super-admin.getPaginatedUsersList");
    Route::post('users', [SuperAdminUserController::class, 'store'])->name("super-admin.storeUser");
    Route::put('users/{user}', [SuperAdminUserController::class, 'update'])->name("super-admin.updateUser");
    Route::delete('user/{user}', [SuperAdminUserController::class, 'delete'])->name("super-admin.deleteUser");
    Route::get('get-visit-purposes-list/{branchOfficeId}', [SuperAdminUserController::class, 'getVisitPurposesList'])->name("super-admin.getVisitPurposesList");
    Route::get('get-branch-office-list/{townId}', [SuperAdminUserController::class, 'getBranchOfficeList'])->name("super-admin.getBranchOfficeList");

    /** Town **/
    Route::get('paginated-towns', [SuperAdminTownController::class, 'getPaginatedTownsList'])->name("super-admin.getPaginatedTownsList");
    Route::post('towns', [SuperAdminTownController::class, 'store'])->name("super-admin.storeTown");
    Route::put('towns/{town}', [SuperAdminTownController::class, 'update'])->name("super-admin.updateTown");
    Route::delete('town/{town}', [SuperAdminTownController::class, 'delete'])->name("super-admin.deleteTown");

    /** Branch Office **/
    Route::get('paginated-branch-offices', [SuperAdminBranchOfficeController::class, 'getPaginatedBranchOfficesList'])->name("super-admin.getPaginatedBranchOfficesList");
    Route::post('branch-offices', [SuperAdminBranchOfficeController::class, 'store'])->name("super-admin.storeBranchOffice");
    Route::put('branch-offices/{branchOffice}', [SuperAdminBranchOfficeController::class, 'update'])->name("super-admin.updateBranchOffice");
    Route::delete('branch-office/{branchOffice}', [SuperAdminBranchOfficeController::class, 'delete'])->name("super-admin.deleteBranchOffice");
    Route::post('/associate/branches', [SuperAdminBranchOfficeController::class, 'associateBranches'])->name("associateBranches");

    /** Service Office **/
    Route::get('paginated-services', [SuperAdminServiceController::class, 'getPaginatedServicesList'])->name("super-admin.getPaginatedServicesList");
    Route::post('services', [SuperAdminServiceController::class, 'store'])->name("super-admin.storeService");
    Route::put('services/{service}', [SuperAdminServiceController::class, 'update'])->name("super-admin.updateService");
    Route::delete('service/{service}', [SuperAdminServiceController::class, 'delete'])->name("super-admin.deleteService");

    /** Visit Purpose Type **/
    Route::get('paginated-visit-purpose-types', [SuperAdminVisitPurposeTypeController::class, 'getPaginatedVisitPurposeTypesList'])->name("super-admin.getPaginatedVisitPurposeTypesList");
    Route::post('visit-purpose-types', [SuperAdminVisitPurposeTypeController::class, 'store'])->name("super-admin.store");
    Route::put('visit-purpose-types/{visitPurposeType}', [SuperAdminVisitPurposeTypeController::class, 'update'])->name("super-admin.update");
    Route::delete('visit-purpose-types/{visitPurposeType}', [SuperAdminVisitPurposeTypeController::class, 'delete'])->name("super-admin.delete");

    /** Visit Purpose **/
    Route::get('paginated-visit-purposes', [SuperAdminVisitPurposeController::class, 'getPaginatedVisitPurposesList'])->name("super-admin.getPaginatedVisitPurposesList");
    Route::post('visit-purposes', [SuperAdminVisitPurposeController::class, 'store'])->name("super-admin.storeVisitPurpose");
    Route::put('visit-purposes/{visitPurpose}', [SuperAdminVisitPurposeController::class, 'update'])->name("super-admin.updateVisitPurpose");
    Route::delete('visit-purpose/{visitPurpose}', [SuperAdminVisitPurposeController::class, 'delete'])->name("super-admin.deleteVisitPurpose");

    /** Sub Visit Purpose **/
    Route::get('paginated-sub-visit-purposes', [SuperAdminSubVisitPurposeController::class, 'getPaginatedSubVisitPurposesList'])->name("super-admin.getSubPaginatedVisitPurposesList");
    Route::post('sub-visit-purposes', [SuperAdminSubVisitPurposeController::class, 'store'])->name("super-admin.storeSubVisitPurpose");
    Route::put('sub-visit-purposes/{subVisitPurpose}', [SuperAdminSubVisitPurposeController::class, 'update'])->name("super-admin.updateSubVisitPurpose");
    Route::delete('sub-visit-purpose/{subVisitPurpose}', [SuperAdminSubVisitPurposeController::class, 'delete'])->name("super-admin.deleteSubVisitPurpose");

    /** Permission **/
    Route::get('paginated-permissions', [SuperAdminPermissionController::class, 'getPaginatedPermissionsList'])->name("super-admin.getPaginatedPermissionsList");
    Route::post('permissions', [SuperAdminPermissionController::class, 'store'])->name("super-admin.storePermission");
    Route::put('permissions/{permission}', [SuperAdminPermissionController::class, 'update'])->name("super-admin.updatePermission");
    Route::delete('permission/{permission}', [SuperAdminPermissionController::class, 'delete'])->name("super-admin.deletePermission");

    /** Role **/
    Route::get('paginated-roles', [SuperAdminRoleController::class, 'getPaginatedRolesList'])->name("super-admin.getPaginatedRolesList");
    Route::post('roles', [SuperAdminRoleController::class, 'store'])->name("super-admin.storeRole");
    Route::put('roles/{role}', [SuperAdminRoleController::class, 'update'])->name("super-admin.updateRole");
    Route::delete('role/{role}', [SuperAdminRoleController::class, 'delete'])->name("super-admin.deleteRole");

    /** Report **/
    Route::prefix('reports')->group(function () {
        Route::get('tickets', [SuperAdminReportController::class, 'getPaginatedTicketsList'])->name("super-admin.reports.getPaginatedTicketsList");
        Route::get('users', [SuperAdminReportController::class, 'getPaginatedUserActionTimesList'])->name("super-admin.reports.getPaginatedUserActionTimesList");
    });

    /** Export **/
    Route::get('tickets/export', [SuperAdminTicketController::class, 'export'])->name("super-admin.tickets.export");
    Route::get('users/export', [SuperAdminUserController::class, 'export'])->name("super-admin.users.export");

});

Route::prefix('hall-admin')->group(function () {
    /** User **/
    Route::put('users/{user}', [HallAdminUserController::class, 'update'])->name("hall-admin.updateUser");
    Route::delete('user/{user}', [HallAdminUserController::class, 'delete'])->name("hall-admin.deleteUser");
    Route::get('users', [HallAdminUserController::class, 'getUsersList'])->name("hall-admin.getUsersList");

    /** TicketController **/
    Route::get('declined-tickets/{branchOfficeId}', [AdminBasketController::class, 'getDeclinedTicketsList'])->name("hall-admin.getDeclinedTicketsList");
    Route::put('recover-ticket/{ticket}', [AdminBasketController::class, 'recoverTicket'])->name("hall-admin.recoverTicket");
});

Route::prefix('admin')->group(function () {
    /** TicketController **/
    Route::get('declined-tickets/{branchOfficeId}', [AdminBasketController::class, 'getDeclinedTicketsList'])->name("admin.getDeclinedTicketsList");
    Route::put('recover-ticket/{ticket}', [AdminBasketController::class, 'recoverTicket'])->name("admin.recoverTicket");
    Route::delete('remove-tickets', [AdminTicketController::class, 'removeTickets'])->name("admin.removeTickets");

});

/** User **/
Route::post('/login', [PersonUserController::class, 'login'])->name('login');
Route::post('/update-work-data', [PersonUserController::class, 'updateWorkData'])->name('updateWorkData');
Route::post('/update-user-status', [PersonUserController::class, 'updateUserStatus'])->name('updateUserStatus');
Route::post('/desk-number-check', [PersonUserController::class, 'isDeskNumberFree'])->name('isDeskNumberFree');
Route::get('/get-work-data', [PersonUserController::class, 'getWorkData'])->name('getWorkData');
Route::get('/get-users-average-service-time', [PersonUserController::class, 'getUsersAverageServiceTime'])->name('getUsersAverageServiceTime');
Route::get('/get-user-average-service-time', [PersonUserController::class, 'getUserAverageServiceTime'])->name('getUserAverageServiceTime');
Route::get('/get-users-max-service-time', [PersonUserController::class, 'getUsersMaxServiceTime'])->name('getUsersMaxServiceTime');
Route::get('/get-customers-amount-user-served', [PersonUserController::class, 'getCustomersAmountUserServed'])->name('getCustomersAmountUserServed');
Route::get('/get-user-data/{id}', [PersonUserController::class, 'getUserData'])->name('getUserData');
Route::post('/update-statistics-data', [PersonUserController::class, 'updateStatisticsData'])->name('updateStatisticsData');

/** TicketController **/
Route::post('/tickets', [TicketController::class, 'store'])->name('storeTicket');
Route::get('/ticket', [TicketController::class, 'getTicket'])->name('getTicket');
Route::get('/full-tickets-list', [TicketController::class, 'getFullTicketsList'])->name('getFullTicketsList');
Route::get('/short-tickets-list', [TicketController::class, 'getShortTicketsList'])->name('getShortTicketsList');
Route::put('/accept-ticket', [TicketController::class, 'acceptTicket'])->name('acceptTicket');
Route::put('/serve-ticket', [TicketController::class, 'serveTicket'])->name('serveTicket');
Route::put('/decline-ticket', [TicketController::class, 'declineTicket'])->name('declineTicket');
Route::put('/complete-service', [TicketController::class, 'completeTicket'])->name('completeTicket');

/** Town **/
Route::get('/towns-list', [PersonTownController::class, 'showTowns'])->name('showTowns');

/** BranchOffice **/
Route::get('/branch-offices-list/{townId}', [PersonBranchOfficeController::class, 'showBranchOffices'])->name('showBranchOffices');

/** Visit Purpose **/
Route::get('/visit-purposes-list/{branchOfficeId}', [PersonVisitPurposeController::class, 'showVisitPurposes'])->name('showVisitPurposes');

/** Sub Visit Purpose **/
Route::get('/sub-visit-purposes-list', [PersonSubVisitPurposeController::class, 'showSubVisitPurposes'])->name('showSubVisitPurposes');

/** Service **/
Route::post('/services-list', [PersonServiceController::class, 'showServices'])->name('showServices');

/** Modules data lists **/
Route::get('users', [ModulesDataListMediator::class, 'getUsersList'])->name("getUsersList");
Route::get('services', [ModulesDataListMediator::class, 'getServicesList'])->name("getServicesList");
Route::get('roles', [ModulesDataListMediator::class, 'getRolesList'])->name("getRolesList");
Route::get('permissions', [ModulesDataListMediator::class, 'getPermissionsList'])->name("getPermissionsList");
Route::get('branch-offices', [ModulesDataListMediator::class, 'getBranchOfficesList'])->name("getBranchOfficesList");
Route::get('towns', [ModulesDataListMediator::class, 'getTownsList'])->name("getTownsList");
Route::get('visit-purposes', [ModulesDataListMediator::class, 'getVisitPurposesList'])->name("getVisitPurposesList");
Route::get('user-statuses', [ModulesDataListMediator::class, 'getUserStatusesList'])->name("getUserStatusesList");
Route::get('visit-purpose-types', [ModulesDataListMediator::class, 'getVisitPurposeTypesList'])->name("getVisitPurposeTypesList");
Route::get('incoming-tickets', [ModulesDataListMediator::class, 'getIncomingTicketsList'])->name("getIncomingTicketsList");

Route::prefix('crm')->group(function () {
    //requests from crm to store ticket and get purposes
    Route::post('/ticket/store', 'App\Http\Controllers\API\TicketController@store')->name('store');
    Route::get('/visit/purposes', [ModulesDataListMediator::class, 'getVisitPurposes'])->name("getVisitPurposes");

    // request from queue front-end to get all crm branches
    Route::post('/branches', [TicketController::class, 'getCRMBranches'])->name('getCRMBranches');
});
